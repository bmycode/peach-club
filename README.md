## 蜜桃社

使用Flutter开发的模特写真App，项目目前开发完成，对Flutter感兴趣的可以先看看代码，支持Linux，Mac，Windows，Web，Android Ios 等多端

> PS: Flutter写的应用也太丝滑太流畅了吧，众多App框架中，只服Flutter！！！

核心代码都在 `/lib` 文件夹中，文件说明：
```text
|____widget  # 公共组件
|____config  # 项目的配置
|____entity  # 数据实体
|____db      # 数据库配置
|____event   # 事件控制中心
|____utils   # 工具函数
|____main.dart  # 主入口
|____R.dart     # 静态资源类，作用类似android中的R文件
|____http       # Dio 的封装
|____static     # 静态资源
|____service    # 请求中间层
|____views      # 主页面
|____router     # 路由配置
|____Configure.dart  # 各种工具和配置的初始化
|____main.dart       # 主启动文件
|____R.dart          # 静态资源配置
```

## 实现功能：
- 搜索
- 分类编辑
- 机构
- 模特
- 收藏写真
- 收藏单图
- 分享
- 保存到相册
- 检测升级
- 最近更新
- 清除缓存
- 极光推送
- 登录 / 注册 / Vip权限控制

等...

看图：

![1.jpg](docs/1.jpg)
![2.jpg](docs/2.jpg)
![3.jpg](docs/3.jpg)
![4.jpg](docs/4.jpg)
![5.jpg](docs/5.jpg)
![6.jpg](docs/6.jpg)
![7.jpg](docs/7.jpg)
![8.jpg](docs/8.jpg)
![9.jpg](docs/9.jpg)
![10.jpg](docs/10.jpg)
