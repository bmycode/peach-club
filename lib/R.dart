class R {
  /// 静态资源配置，程序自动生成，请勿手动修改
  /// ![](http://127.0.0.1:2227/lib/static/img/update_ic_close.png)
  static final String libStaticImgUpdateIcClosePng = 'lib/static/img/update_ic_close.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/taxi.png)
  static final String libStaticImgTaxiPng = 'lib/static/img/taxi.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/.DS_Store)
  static final String libStaticImgDsStore = 'lib/static/img/.DS_Store';
  /// ![](http://127.0.0.1:2227/lib/static/img/welcome3.png)
  static final String libStaticImgWelcome3Png = 'lib/static/img/welcome3.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/welcome2.png)
  static final String libStaticImgWelcome2Png = 'lib/static/img/welcome2.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/update_bg_app_top.png)
  static final String libStaticImgUpdateBgAppTopPng = 'lib/static/img/update_bg_app_top.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/logo.png)
  static final String libStaticImgLogoPng = 'lib/static/img/logo.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/vipLogo.png)
  static final String libStaticImgViplogoPng = 'lib/static/img/vipLogo.png';
  /// ![](http://127.0.0.1:2227/lib/static/img/back.png)
  static final String libStaticImgBackPng = 'lib/static/img/back.png';
}
