import 'package:flutter/material.dart';

class ColorConfig {

  // 主题色
  static const Color ThemeColor = Color(0XFFfb7299);
  // static const Color ThemeColor = Color(0XFFffd101);
  // 比主题稍微淡一点的主题色
  static const Color ThemeOtherColor = Color(0XFFfe7fa2);
  // cell 左侧图标的颜色
  static const Color IconColor = Color(0XFFc3c0c0);
  // 某些标题的黑色
  static const Color TitleColor = Color(0XFF333333);
  // 某些稍微灰色一点的字体颜色
  static const Color TextColor = Color(0XFF7e7979);
  // 未激活时候的字体颜色
  static const Color NoActiveColor = Color(0XFFd3d2d2);
  static const Color LineColor = Color(0XFFefefef);
  // 白色背景
  static const Color WhiteBackColor = Colors.white;

}