import 'dart:io';
import 'package:event_bus/event_bus.dart';
import 'package:package_info/package_info.dart';
import 'package:device_info/device_info.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:flutter/material.dart';
import 'package:peach/R.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/utils/_.dart';

class Config {

  /// 第一次打开，本地无值，返回false取反为true所以显示welcome
  /// 第二次打开，本地有值，返回true取反false，所以显示app页面
  static bool isFirstOpen = false;

  // 第一次打开，未登录，本地无值，返回false，取反为true，访问登录页面
  // 第二次打开，已登录，本地有值，返回true，取反为返回false，访问app页面
  static bool isLogin = true;

  /// App 的名字
  static String appName = "屁桃儿";
  /// 保存 App安装包信息
  static PackageInfo packageInfo;

  /// 是否为 android
  static bool isAndroid = Platform.isAndroid;
  /// 保存 android 设备信息
  static AndroidDeviceInfo androidDeviceInfo;

  /// 是否为 ios
  static bool isIos = Platform.isIOS;
  /// 保存 ios 设备信息
  static IosDeviceInfo iosDeviceInfo;
  /// IOS app 下载地址
  // static String iosDownApkUrl = "https://apps.apple.com/cn/app/qq/id444934666";
  static String iosDownApkUrl = "itms-apps://itunes.apple.com/cn/app/id444934666";

  static int currentIndex = 0;

  // 保存视频数组
  static List<Feeds> feeds = [];

  /// 首页 /home.html
  static String h5IpUrl = "http://47.114.153.249/page/";
  static String shareIpUrl = "http://47.114.153.249/share/";
  static String h5DomainUrl = "http://www.geekhelp.cn/page/";

  /// 极光推送的配置
  static final Map<String, dynamic> JgConfig = {
    "appKey": 'd4e22ab876276f4a374880ca',
    "channel": 'pitaoer',
    "production": true,
    "debug": false,
  };

  /// 首页底部Tab的控制器
  static PageController tabController;

  /// 事件控制中心
  static EventBus eventBus;

  static List vipList = [
    {
      "id": 1,
      "vipTitle": "月卡",
      "vipMoney": 30,
      "vipDay": 1,
      "vipExplain": [
        "1：✅ 可以收藏（单图收藏&写真收藏）",
        "2：❎ 无法保存图片到相册",
        "3：❎ 无法使用【成人视频】功能",
        "4：❎ 无法下载【成人视频】功能"
      ]
    },
    {
      "id": 2,
      "vipTitle": "季卡",
      "vipMoney": 88,
      "vipDay": 0.9,
      "vipExplain": [
        "1：✅ 可以收藏（单图收藏&写真收藏）",
        "2：✅ 可以下载保存到相册",
        "3：❎ 无法使用【成人视频】功能",
        "4：❎ 无法下载【成人视频】功能"
      ]
    },
    {
      "id": 3,
      "vipTitle": "年卡",
      "vipMoney": 324,
      "vipDay": 0.8,
      "vipExplain": [
        "1：✅ 可以收藏（单图收藏&写真收藏）",
        "2：✅ 可以下载保存到相册",
        "3：✅ 可以使用【成人视频】功能",
        "4：✅ 可以下载【成人视频】功能"
      ]
    },
  ];

  /// 接口地址
  static final Map<String, dynamic> ApiUrl = {

    // 写真图片接口基地址
    'PhotoBaseUrl': 'http://192.168.0.105:3000/api/json',
    // 'PhotoBaseUrl': 'http://120.27.144.174:3000/api/json',

    // 'VideoBaseUrl': 'http://47.114.153.249:1314',
    'VideoBaseUrl': 'http://192.168.0.105:8090/api',

    // 视频接口基地址
    'ApiList': {
      'update': '/updateVersion', // App升级接口
      'index': '/index', // 首页
      'class': '/class', // 获取分类菜单
      'login': '/login', // 登录
      'register': '/register', // 注册
      'agentreg': '/agentreg', // 代理注册
      'freeUse': '/freeUse', // vip计数
      'place': '/placeOrder', // 办理vip
      'orderStatus': '/orderStatus', // 查询购买记录

      'agentInfo': '/agent/info', // 【新接口，替换】代替原本的详情
      'info': '/info', // 进入详情

      'classList': '/classList', // 获取分类列表下数据
      'agentClassList': '/agent/classList', //【新接口，替换】 代替原本的 获取分类列表下数据

      'mechanism': '/mechanism', // 机构列表

      'mechanismList': '/mechanismList', // 机构列表点击进入详情
      'agentMechanismList': '/agent/mechanismList',  // 【新接口，替换】机构列表点击进入详情

      'allModel': '/allModel', // 模特列表
      'modelList': '/modelList', // 进入模特的个人套图页

      'agentRecently': '/agent/recently', // 最近更新
      'agentSearch': '/agent/search', // 搜索接口

      'agentRandom': '/agent/random', // 随机展示


      'videoType': '/likeList'
    }
  };

  static final pages = [
    PageViewModel(
        pageColor: ColorConfig.ThemeColor,
        body: Text(
          '一款正经的模特写真应用',
          style: TextStyle(fontSize: Screen.setFontSize(17.0)),
        ),
        title: Text(
          Config.appName,
          style: TextStyle(fontSize: Screen.setFontSize(30.0)),
        ),
        textStyle: TextStyle(color: Colors.white),
        mainImage: Image.asset(
          R.libStaticImgLogoPng,
          height: Screen.setHeight(225.0),
          width: Screen.setWidth(225.0),
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: const Color(0xFF03A9F4),
        body: Text(
          '基于谷歌 Flutter+Dart 开发，体验更丝滑更流畅，响应速度让人惊叹',
          style: TextStyle(fontSize: Screen.setFontSize(17.0)),
        ),
        title: Text(
          '流畅极致',
          style: TextStyle(fontSize: Screen.setFontSize(30.0)),
        ),
        textStyle: TextStyle(color: Colors.white),
        mainImage: Image.asset(
          R.libStaticImgWelcome2Png,
          height: Screen.setHeight(255.0),
          width: Screen.setWidth(255.0),
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF8BC34A),
      body: Text(
        '国内外100多家写真机构，198万张高清模特写真，有你喜欢的',
        style: TextStyle(fontSize: Screen.setFontSize(17.0)),
      ),
      title: Text(
        '全面丰富',
        style: TextStyle(fontSize: Screen.setFontSize(30.0)),
      ),
      mainImage: Image.asset(
        R.libStaticImgWelcome3Png,
        height: Screen.setHeight(255.0),
        width: Screen.setWidth(255.0),
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFF607D8B),
      body: Text(
        '点击【进入】开始使用！',
        style: TextStyle(fontSize: Screen.setFontSize(17.0)),
      ),
      title: Text(
        '开始使用',
        style: TextStyle(fontSize: Screen.setFontSize(30.0)),
      ),
      mainImage: Image.asset(
        R.libStaticImgTaxiPng,
        height: Screen.setHeight(255.0),
        width: Screen.setWidth(255.0),
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(color: Colors.white),
    ),
  ];
}