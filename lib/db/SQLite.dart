import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SQLite {
  static final SQLite _singleton = SQLite._internal();
  factory SQLite() => _singleton;
  SQLite._internal();
  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await _initDB();
    return _db;
  }

  Future<Database> _initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'MiTaoShe');
    print("documentsDirectory.path ============ : ${path}");
    return await openDatabase(
        path,
        version: 1,
        onCreate: _onCreate
    );
  }


  Future _onCreate(Database db, int version) async {
    /// 创建like 喜欢表
     await db.execute("CREATE TABLE Like ("
        "id integer primary key AUTOINCREMENT,"
        "preview TEXT,"
        "source TEXT,"
        "title TEXT,"
        "totals TEXT,"
        "user TEXT"
     ")");

     /// 创建Menu 菜单 Principal(默认菜单) 表
     await db.execute("CREATE TABLE Principal ("
         "id integer primary key AUTOINCREMENT,"
         "title TEXT,"
         "page integer"
     ")");

     /// 创建Menu 菜单 Additional(可选菜单) 表
     await db.execute("CREATE TABLE Additional ("
         "id integer primary key AUTOINCREMENT,"
         "title TEXT,"
         "page integer"
     ")");

     /// 创建 单图收藏 表
     await db.execute("CREATE TABLE SingleGraph ("
         "id integer primary key AUTOINCREMENT,"
         "src TEXT"
     ")");
  }

}