import 'AgentInfoEntity.dart';

class AgentClassListEntity {
  int status;
  AgentClassListData data;

  AgentClassListEntity({this.status, this.data});

  AgentClassListEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new AgentClassListData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AgentClassListData {
  dynamic total;
  int num;
  List<ItemList> list;
  String page;

  AgentClassListData({this.total, this.num, this.list, this.page});

  AgentClassListData.fromJson(Map<String, dynamic> json) {
      total = json['total'];
    num = json['num'];
    page = json['page'];
    if (json['list'] != null) {
      list = new List<ItemList>();
      json['list'].forEach((v) {
        list.add(new ItemList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    data['num'] = this.num;
    data['page'] = this.page;
    if (this.list != null) {
      data['list'] = this.list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ItemList {
  String id;
  String catid;
  String title;
  String renwu;
  String biaoqian;
  String jigou;
  String bianhao;
  String faxingshijian;
  String shuliang;
  String zhuangtai;
  String shijian;
  String tuijian;
  String classname;
  List<Biaoqianlist> biaoqianlist;
  List<Biaoqianlist> jigoulist;
  String renwunlist;
  String isgood;
  String imgSrc;
  List<Biaoqianlist> renwulist;

  ItemList(
      {this.id,
        this.catid,
        this.title,
        this.renwu,
        this.biaoqian,
        this.jigou,
        this.bianhao,
        this.faxingshijian,
        this.shuliang,
        this.zhuangtai,
        this.shijian,
        this.tuijian,
        this.classname,
        this.biaoqianlist,
        this.jigoulist,
        this.renwunlist,
        this.isgood,
        this.imgSrc,
        this.renwulist});

  ItemList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catid = json['catid'];
    title = json['title'];
    renwu = json['renwu'];
    biaoqian = json['biaoqian'];
    jigou = json['jigou'];
    bianhao = json['bianhao'];
    faxingshijian = json['faxingshijian'];
    shuliang = json['shuliang'];
    zhuangtai = json['zhuangtai'];
    shijian = json['shijian'];
    tuijian = json['tuijian'];
    classname = json['classname'];
    if (json['biaoqianlist'] != null) {
      biaoqianlist = new List<Biaoqianlist>();
      json['biaoqianlist'].forEach((v) {
        biaoqianlist.add(new Biaoqianlist.fromJson(v));
      });
    }
    if (json['jigoulist'] != null) {
      jigoulist = new List<Biaoqianlist>();
      json['jigoulist'].forEach((v) {
        jigoulist.add(new Biaoqianlist.fromJson(v));
      });
    }
    renwunlist = json['renwunlist'];
    isgood = json['isgood'];
    imgSrc = json['img_src'];
    if (json['renwulist'] != null) {
      renwulist = new List<Biaoqianlist>();
      json['renwulist'].forEach((v) {
        renwulist.add(new Biaoqianlist.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['catid'] = this.catid;
    data['title'] = this.title;
    data['renwu'] = this.renwu;
    data['biaoqian'] = this.biaoqian;
    data['jigou'] = this.jigou;
    data['bianhao'] = this.bianhao;
    data['faxingshijian'] = this.faxingshijian;
    data['shuliang'] = this.shuliang;
    data['zhuangtai'] = this.zhuangtai;
    data['shijian'] = this.shijian;
    data['tuijian'] = this.tuijian;
    data['classname'] = this.classname;
    if (this.biaoqianlist != null) {
      data['biaoqianlist'] = this.biaoqianlist.map((v) => v.toJson()).toList();
    }
    if (this.jigoulist != null) {
      data['jigoulist'] = this.jigoulist.map((v) => v.toJson()).toList();
    }
    data['renwunlist'] = this.renwunlist;
    data['isgood'] = this.isgood;
    data['img_src'] = this.imgSrc;
    if (this.renwulist != null) {
      data['renwulist'] = this.renwulist.map((v) => v.toJson()).toList();
    }
    return data;
  }
}