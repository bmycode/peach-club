class AgentInfoEntity {
  int status;
  AgentInfo data;

  AgentInfoEntity({this.status, this.data});

  AgentInfoEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new AgentInfo.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AgentInfo {
  String id;
  String catid;
  String title;
  String renwu;
  String biaoqian;
  String jigou;
  String bianhao;
  String faxingshijian;
  String shuliang;
  String zhuangtai;
  String shijian;
  String tuijian;
  String classname;
  List<Biaoqianlist> biaoqianlist;
  List<Biaoqianlist> jigoulist;
  String renwunlist;
  String isgood;
  String imgSrc;
  List<Biaoqianlist> renwulist;
  List<String> imgs;

  AgentInfo(
      {this.id,
        this.catid,
        this.title,
        this.renwu,
        this.biaoqian,
        this.jigou,
        this.bianhao,
        this.faxingshijian,
        this.shuliang,
        this.zhuangtai,
        this.shijian,
        this.tuijian,
        this.classname,
        this.biaoqianlist,
        this.jigoulist,
        this.renwunlist,
        this.isgood,
        this.imgSrc,
        this.renwulist,
        this.imgs});

  AgentInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catid = json['catid'];
    title = json['title'];
    renwu = json['renwu'];
    biaoqian = json['biaoqian'];
    jigou = json['jigou'];
    bianhao = json['bianhao'];
    faxingshijian = json['faxingshijian'];
    shuliang = json['shuliang'];
    zhuangtai = json['zhuangtai'];
    shijian = json['shijian'];
    tuijian = json['tuijian'];
    classname = json['classname'];
    if (json['biaoqianlist'] != null) {
      biaoqianlist = new List<Biaoqianlist>();
      json['biaoqianlist'].forEach((v) {
        biaoqianlist.add(new Biaoqianlist.fromJson(v));
      });
    }
    if (json['jigoulist'] != null) {
      jigoulist = new List<Biaoqianlist>();
      json['jigoulist'].forEach((v) {
        jigoulist.add(new Biaoqianlist.fromJson(v));
      });
    }
    renwunlist = json['renwunlist'];
    isgood = json['isgood'];
    imgSrc = json['img_src'];
    if (json['renwulist'] != null) {
      renwulist = new List<Biaoqianlist>();
      json['renwulist'].forEach((v) {
        renwulist.add(new Biaoqianlist.fromJson(v));
      });
    }
    imgs = json['imgs'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['catid'] = this.catid;
    data['title'] = this.title;
    data['renwu'] = this.renwu;
    data['biaoqian'] = this.biaoqian;
    data['jigou'] = this.jigou;
    data['bianhao'] = this.bianhao;
    data['faxingshijian'] = this.faxingshijian;
    data['shuliang'] = this.shuliang;
    data['zhuangtai'] = this.zhuangtai;
    data['shijian'] = this.shijian;
    data['tuijian'] = this.tuijian;
    data['classname'] = this.classname;
    if (this.biaoqianlist != null) {
      data['biaoqianlist'] = this.biaoqianlist.map((v) => v.toJson()).toList();
    }
    if (this.jigoulist != null) {
      data['jigoulist'] = this.jigoulist.map((v) => v.toJson()).toList();
    }
    data['renwunlist'] = this.renwunlist;
    data['isgood'] = this.isgood;
    data['img_src'] = this.imgSrc;
    if (this.renwulist != null) {
      data['renwulist'] = this.renwulist.map((v) => v.toJson()).toList();
    }
    data['imgs'] = this.imgs;
    return data;
  }
}

class Biaoqianlist {
  dynamic id;
  String name;

  Biaoqianlist({this.id, this.name});

  Biaoqianlist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}