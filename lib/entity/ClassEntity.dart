class ClassEntity {
  int _status;
  String _msg;
  ClassResult _result;

  ClassEntity({int status, String msg, ClassResult result}) {
    this._status = status;
    this._msg = msg;
    this._result = result;
  }

  int get status => _status;
  set status(int status) => _status = status;
  String get msg => _msg;
  set msg(String msg) => _msg = msg;
  ClassResult get result => _result;
  set result(ClassResult result) => _result = result;

  ClassEntity.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _msg = json['msg'];
    _result =
    json['result'] != null ? new ClassResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['msg'] = this._msg;
    if (this._result != null) {
      data['result'] = this._result.toJson();
    }
    return data;
  }
}

class ClassResult {
  List<Principal> _principal;
  List<Principal> _additional;

  Result({List<Principal> principal, List<Principal> additional}) {
    this._principal = principal;
    this._additional = additional;
  }

  List<Principal> get principal => _principal;
  set principal(List<Principal> principal) => _principal = principal;
  List<Principal> get additional => _additional;
  set additional(List<Principal> additional) => _additional = additional;

  ClassResult.fromJson(Map<String, dynamic> json) {
    if (json['principal'] != null) {
      _principal = new List<Principal>();
      json['principal'].forEach((v) {
        _principal.add(new Principal.fromJson(v));
      });
    }
    if (json['additional'] != null) {
      _additional = new List<Principal>();
      json['additional'].forEach((v) {
        _additional.add(new Principal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._principal != null) {
      data['principal'] = this._principal.map((v) => v.toJson()).toList();
    }
    if (this._additional != null) {
      data['additional'] = this._additional.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Principal {
  int _id;
  String _title;
  int _page;

  Principal({int id, String title, int page}) {
    this._id = id;
    this._title = title;
    this._page = page;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  int get page => _page;
  set page(int page) => _page = page;

  Principal.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _page = json['page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['page'] = this._page;
    return data;
  }
}