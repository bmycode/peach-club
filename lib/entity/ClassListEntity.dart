import 'package:peach/entity/_.dart';

class ClassListEntity {
  int status;
  String msg;
  ClassListResult result;

  ClassListEntity({this.status, this.msg, this.result});

  ClassListEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    result =
    json['result'] != null ? new ClassListResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class ClassListResult {
  String group;
  int currentPage;
  int totalPage;
  List<HomeEntityData> data;

  ClassListResult({this.group, this.currentPage, this.totalPage, this.data});

  ClassListResult.fromJson(Map<String, dynamic> json) {
    group = json['group'];
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    if (json['data'] != null) {
      data = new List<HomeEntityData>();
      json['data'].forEach((v) {
        data.add(new HomeEntityData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['group'] = this.group;
    data['current_page'] = this.currentPage;
    data['total_page'] = this.totalPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ClassTags {
  String title;
  int id;

  ClassTags({this.title, this.id});

  ClassTags.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['id'] = this.id;
    return data;
  }
}