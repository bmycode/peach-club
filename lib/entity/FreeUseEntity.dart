class FreeUseEntity {
  int _code;
  String _error;
  int _freeNumber;

  FreeUseEntity({int code, String error, int freeNumber}) {
    this._code = code;
    this._error = error;
    this._freeNumber = freeNumber;
  }

  int get code => _code;
  set code(int code) => _code = code;
  String get error => _error;
  set error(String error) => _error = error;
  int get freeNumber => _freeNumber;
  set freeNumber(int freeNumber) => _freeNumber = freeNumber;

  FreeUseEntity.fromJson(Map<String, dynamic> json) {
    _code = json['code'];
    _error = json['error'];
    _freeNumber = json['FreeNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this._code;
    data['error'] = this._error;
    data['FreeNumber'] = this._freeNumber;
    return data;
  }
}
