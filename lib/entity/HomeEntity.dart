class HomeEntity {
  int status;
  String msg;
  Result result;

  HomeEntity({this.status, this.msg, this.result});
  HomeEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class Result {
  Recommended recommended;
  Recommended newest;
  Models model;

  Result({this.recommended, this.newest, this.model});

  Result.fromJson(Map<String, dynamic> json) {
    recommended = json['recommended'] != null
        ? new Recommended.fromJson(json['recommended'])
        : null;
    newest = json['newest'] != null
        ? new Recommended.fromJson(json['newest'])
        : null;
    model = json['model'] != null ? new Models.fromJson(json['model']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.recommended != null) {
      data['recommended'] = this.recommended.toJson();
    }
    if (this.newest != null) {
      data['newest'] = this.newest.toJson();
    }
    if (this.model != null) {
      data['model'] = this.model.toJson();
    }
    return data;
  }
}

class Recommended {
  String title;
  List<HomeEntityData> data;

  Recommended({this.title, this.data});

  Recommended.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    if (json['data'] != null) {
      data = new List<HomeEntityData>();
      json['data'].forEach((v) {
        data.add(new HomeEntityData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeEntityData {
  int id;
  int total;
  int totalPage;
  String source;
  String user;
  List<Tags> tags;
  String title;
  String preview;

  HomeEntityData(
      {this.id,
        this.total,
        this.totalPage,
        this.source,
        this.user,
        this.tags,
        this.title,
        this.preview});

  HomeEntityData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    total = json['total'];
    totalPage = json['total_page'];
    source = json['source'];
    user = json['user'];
    if (json['tags'] != null) {
      tags = new List<Tags>();
      json['tags'].forEach((v) {
        tags.add(new Tags.fromJson(v));
      });
    }
    title = json['title'];
    preview = json['preview'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['total'] = this.total;
    data['total_page'] = this.totalPage;
    data['source'] = this.source;
    data['user'] = this.user;
    if (this.tags != null) {
      data['tags'] = this.tags.map((v) => v.toJson()).toList();
    }
    data['title'] = this.title;
    data['preview'] = this.preview;
    return data;
  }
}

class Tags {
  String title;
  int id;

  Tags({this.title, this.id});

  Tags.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['id'] = this.id;
    return data;
  }
}

class Models {
  String title;
  List<Res> res;

  Models({this.title, this.res});

  Models.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    if (json['res'] != null) {
      res = new List<Res>();
      json['res'].forEach((v) {
        res.add(new Res.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    if (this.res != null) {
      data['res'] = this.res.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Res {
  int id;
  String title;
  String total;
  String preview;

  Res({this.id, this.title, this.total, this.preview});

  Res.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    total = json['total'];
    preview = json['preview'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['total'] = this.total;
    data['preview'] = this.preview;
    return data;
  }
}