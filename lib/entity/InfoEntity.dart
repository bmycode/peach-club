import 'HomeEntity.dart';

class InfoEntity {
  int status;
  String msg;
  InfoResult result;

  InfoEntity({this.status, this.msg, this.result});

  InfoEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    result =
    json['result'] != null ? new InfoResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class InfoResult {
  String title;
  String source;
  String total;
  String info;
  int currentPage;
  int totalPage;
  List<Data> data;
  List<HomeEntityData> recommend;
  List<HomeEntityData> relevant;

  InfoResult(
      {this.title,
        this.source,
        this.total,
        this.info,
        this.currentPage,
        this.totalPage,
        this.data,
        this.recommend,
        this.relevant});

  InfoResult.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    source = json['source'];
    total = json['total'];
    info = json['info'];
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    if (json['recommend'] != null) {
      recommend = new List<HomeEntityData>();
      json['recommend'].forEach((v) {
        recommend.add(new HomeEntityData.fromJson(v));
      });
    }
    if (json['relevant'] != null) {
      relevant = new List<HomeEntityData>();
      json['relevant'].forEach((v) {
        relevant.add(new HomeEntityData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['source'] = this.source;
    data['total'] = this.total;
    data['info'] = this.info;
    data['current_page'] = this.currentPage;
    data['total_page'] = this.totalPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.recommend != null) {
      data['recommend'] = this.recommend.map((v) => v.toJson()).toList();
    }
    if (this.relevant != null) {
      data['relevant'] = this.relevant.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class Data {
  String title;
  String src;

  Data({this.title, this.src});

  Data.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    src = json['src'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['src'] = this.src;
    return data;
  }
}
