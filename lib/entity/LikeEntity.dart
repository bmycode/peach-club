import 'HomeEntity.dart';

class LikeEntity extends HomeEntityData {
  int id;
  String preview;
  String source;
  String title;
  String totals;
  String user;

  LikeEntity({this.id, this.preview, this.source, this.title, this.totals, this.user});

  LikeEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    preview = json['preview'];
    source = json['source'];
    title = json['title'];
    totals = json['totals'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['preview'] = this.preview;
    data['source'] = this.source;
    data['title'] = this.title;
    data['totals'] = this.totals;
    data['user'] = this.user;
    return data;
  }
}

class SingleGraphEntity {
  int id;
  String src;

  SingleGraphEntity({this.id, this.src});

  SingleGraphEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    src = json['src'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['src'] = this.src;
    return data;
  }
}