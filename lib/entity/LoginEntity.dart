class LoginEntity {
  int code;
  String error;
  String username;
  String createdAt;
  String objectId;
  String userType;
  String userWeiXin;


  LoginEntity({this.code, this.error, this.username, this.createdAt, this.objectId, this.userType, this.userWeiXin});

  LoginEntity.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    error = json['error'];
    username = json['username'];
    createdAt = json['createdAt'];
    objectId = json['objectId'];
    userType = json['userType'];
    userWeiXin = json['userWeiXin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['error'] = this.error;
    data['username'] = this.username;
    data['createdAt'] = this.createdAt;
    data['objectId'] = this.objectId;
    data['userType'] = this.userType;
    data['userWeiXin'] = this.userWeiXin;
    return data;
  }
}