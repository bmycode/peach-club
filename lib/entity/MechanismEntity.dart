class MechanismEntity {
  int status;
  String msg;
  List<MechanismResult> result;

  MechanismEntity({this.status, this.msg, this.result});

  MechanismEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    if (json['result'] != null) {
      result = new List<MechanismResult>();
      json['result'].forEach((v) {
        result.add(new MechanismResult.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MechanismResult {
  int id;
  String title;
  String quantity;

  MechanismResult({this.id, this.title, this.quantity});

  MechanismResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['quantity'] = this.quantity;
    return data;
  }
}