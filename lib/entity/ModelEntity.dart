import 'HomeEntity.dart';

class ModelEntity {
  int status;
  String msg;
  ModelResult result;

  ModelEntity({this.status, this.msg, this.result});

  ModelEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    result =
    json['result'] != null ? new ModelResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class ModelResult {
  List<Res> hotModels;
  List<Res> newest;

  ModelResult({this.hotModels, this.newest});

  ModelResult.fromJson(Map<String, dynamic> json) {
    if (json['hot_models'] != null) {
      hotModels = new List<Res>();
      json['hot_models'].forEach((v) {
        hotModels.add(new Res.fromJson(v));
      });
    }
    if (json['newest'] != null) {
      newest = new List<Res>();
      json['newest'].forEach((v) {
        newest.add(new Res.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.hotModels != null) {
      data['hot_models'] = this.hotModels.map((v) => v.toJson()).toList();
    }
    if (this.newest != null) {
      data['newest'] = this.newest.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HotModels {
  int id;
  String title;
  String total;
  String preview;

  HotModels({this.id, this.title, this.total, this.preview});

  HotModels.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    total = json['total'];
    preview = json['preview'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['total'] = this.total;
    data['preview'] = this.preview;
    return data;
  }
}