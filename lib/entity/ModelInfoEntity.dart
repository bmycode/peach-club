import 'HomeEntity.dart';

class ModelInfoEntity {
  int status;
  String msg;
  ModelInfoResult result;

  ModelInfoEntity({this.status, this.msg, this.result});

  ModelInfoEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    msg = json['msg'];
    result =
    json['result'] != null ? new ModelInfoResult.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['msg'] = this.msg;
    if (this.result != null) {
      data['result'] = this.result.toJson();
    }
    return data;
  }
}

class ModelInfoResult {
  int currentPage;
  int totalPage;
  ModelInfo modelInfo;
  List<HomeEntityData> data;
  List<Res> similar;

  ModelInfoResult(
      {this.currentPage,
        this.totalPage,
        this.modelInfo,
        this.data,
        this.similar});

  ModelInfoResult.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    totalPage = json['total_page'];
    modelInfo = json['model_info'] != null
        ? new ModelInfo.fromJson(json['model_info'])
        : null;
    if (json['data'] != null) {
      data = new List<HomeEntityData>();
      json['data'].forEach((v) {
        data.add(new HomeEntityData.fromJson(v));
      });
    }
    if (json['similar'] != null) {
      similar = new List<Res>();
      json['similar'].forEach((v) {
        similar.add(new Res.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    data['total_page'] = this.totalPage;
    if (this.modelInfo != null) {
      data['model_info'] = this.modelInfo.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.similar != null) {
      data['similar'] = this.similar.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ModelInfo {
  String portrait;
  String name;
  String explain;
  String total;

  ModelInfo({this.portrait, this.name, this.explain, this.total});

  ModelInfo.fromJson(Map<String, dynamic> json) {
    portrait = json['portrait'];
    name = json['name'];
    explain = json['explain'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['portrait'] = this.portrait;
    data['name'] = this.name;
    data['explain'] = this.explain;
    data['total'] = this.total;
    return data;
  }
}
