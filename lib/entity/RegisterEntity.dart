class RegisterEntity {
  int _code;
  String _error;
  String _createdAt;
  String _objectId;

  RegisterEntity({int code, String error, String createdAt, String objectId}) {
    this._code = code;
    this._error = error;
    this._createdAt = createdAt;
    this._objectId = objectId;
  }

  int get code => _code;
  set code(int code) => _code = code;
  String get error => _error;
  set error(String error) => _error = error;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get objectId => _objectId;
  set objectId(String objectId) => _objectId = objectId;

  RegisterEntity.fromJson(Map<String, dynamic> json) {
    _code = json['code'];
    _error = json['error'];
    _createdAt = json['createdAt'];
    _objectId = json['objectId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this._code;
    data['error'] = this._error;
    data['createdAt'] = this._createdAt;
    data['objectId'] = this._objectId;
    return data;
  }
}