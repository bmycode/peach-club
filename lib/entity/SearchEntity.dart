import 'AgentClassListEntity.dart';

class SearchEntity {
  int status;
  List<ItemList> data;

  SearchEntity({this.status, this.data});

  SearchEntity.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<ItemList>();
      json['data'].forEach((v) {
        data.add(new ItemList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }

}