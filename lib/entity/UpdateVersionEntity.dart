class UpdateVersionEntity {
  String createdAt;
  String downUrl;
  String objectId;
  List<String> updateMsg;
  String updatedAt;
  String version;
  int versionCode;

  UpdateVersionEntity(
      {this.createdAt,
        this.downUrl,
        this.objectId,
        this.updateMsg,
        this.updatedAt,
        this.version,
        this.versionCode});

  UpdateVersionEntity.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    downUrl = json['downUrl'];
    objectId = json['objectId'];
    updateMsg = json['updateMsg'].cast<String>();
    updatedAt = json['updatedAt'];
    version = json['version'];
    versionCode = json['versionCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdAt'] = this.createdAt;
    data['downUrl'] = this.downUrl;
    data['objectId'] = this.objectId;
    data['updateMsg'] = this.updateMsg;
    data['updatedAt'] = this.updatedAt;
    data['version'] = this.version;
    data['versionCode'] = this.versionCode;
    return data;
  }
}
