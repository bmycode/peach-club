class VideoListEntity {
  int _result;
  String _llsid;
  String _webPageArea;
  List<Feeds> _feeds;
  Null _hostName;
  String _pcursor;
  String _sTypename;

  VideoListEntity(
      {int result,
        String llsid,
        String webPageArea,
        List<Feeds> feeds,
        Null hostName,
        String pcursor,
        String sTypename}) {
    this._result = result;
    this._llsid = llsid;
    this._webPageArea = webPageArea;
    this._feeds = feeds;
    this._hostName = hostName;
    this._pcursor = pcursor;
    this._sTypename = sTypename;
  }

  int get result => _result;
  set result(int result) => _result = result;
  String get llsid => _llsid;
  set llsid(String llsid) => _llsid = llsid;
  String get webPageArea => _webPageArea;
  set webPageArea(String webPageArea) => _webPageArea = webPageArea;
  List<Feeds> get feeds => _feeds;
  set feeds(List<Feeds> feeds) => _feeds = feeds;
  Null get hostName => _hostName;
  set hostName(Null hostName) => _hostName = hostName;
  String get pcursor => _pcursor;
  set pcursor(String pcursor) => _pcursor = pcursor;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  VideoListEntity.fromJson(Map<String, dynamic> json) {
    _result = json['result'];
    _llsid = json['llsid'];
    _webPageArea = json['webPageArea'];
    if (json['feeds'] != null) {
      _feeds = new List<Feeds>();
      json['feeds'].forEach((v) {
        _feeds.add(new Feeds.fromJson(v));
      });
    }
    _hostName = json['hostName'];
    _pcursor = json['pcursor'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['result'] = this._result;
    data['llsid'] = this._llsid;
    data['webPageArea'] = this._webPageArea;
    if (this._feeds != null) {
      data['feeds'] = this._feeds.map((v) => v.toJson()).toList();
    }
    data['hostName'] = this._hostName;
    data['pcursor'] = this._pcursor;
    data['__typename'] = this._sTypename;
    return data;
  }
}

class Feeds {
  int _type;
  Author _author;
  List<Tag> _tag;
  Photo _photo;
  int _canAddComment;
  String _currentPcursor;
  String _llsid;
  int _status;
  String _sTypename;

  Feeds(
      {int type,
        Author author,
        List<Tag> tag,
        Photo photo,
        int canAddComment,
        String currentPcursor,
        String llsid,
        int status,
        String sTypename}) {
    this._type = type;
    this._author = author;
    this._tag = tag;
    this._photo = photo;
    this._canAddComment = canAddComment;
    this._currentPcursor = currentPcursor;
    this._llsid = llsid;
    this._status = status;
    this._sTypename = sTypename;
  }

  int get type => _type;
  set type(int type) => _type = type;
  Author get author => _author;
  set author(Author author) => _author = author;
  List<Tag> get tag => _tag;
  set tag(List<Tag> tag) => _tag = tag;
  Photo get photo => _photo;
  set photo(Photo photo) => _photo = photo;
  int get canAddComment => _canAddComment;
  set canAddComment(int canAddComment) => _canAddComment = canAddComment;
  String get currentPcursor => _currentPcursor;
  set currentPcursor(String currentPcursor) => _currentPcursor = currentPcursor;
  String get llsid => _llsid;
  set llsid(String llsid) => _llsid = llsid;
  int get status => _status;
  set status(int status) => _status = status;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  Feeds.fromJson(Map<String, dynamic> json) {
    _type = json['type'];
    _author =
    json['author'] != null ? new Author.fromJson(json['author']) : null;
    if (json['tag'] != null) {
      _tag = new List<Tag>();
      json['tag'].forEach((v) {
        _tag.add(new Tag.fromJson(v));
      });
    }
    _photo = json['photo'] != null ? new Photo.fromJson(json['photo']) : null;
    _canAddComment = json['canAddComment'];
    _currentPcursor = json['currentPcursor'];
    _llsid = json['llsid'];
    _status = json['status'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this._type;
    if (this._author != null) {
      data['author'] = this._author.toJson();
    }
    if (this._tag != null) {
      data['tag'] = this._tag.map((v) => v.toJson()).toList();
    }
    if (this._photo != null) {
      data['photo'] = this._photo.toJson();
    }
    data['canAddComment'] = this._canAddComment;
    data['currentPcursor'] = this._currentPcursor;
    data['llsid'] = this._llsid;
    data['status'] = this._status;
    data['__typename'] = this._sTypename;
    return data;
  }
}

class Author {
  String _id;
  String _name;
  bool _following;
  String _headerUrl;
  Null _headerUrls;
  String _sTypename;

  Author(
      {String id,
        String name,
        bool following,
        String headerUrl,
        Null headerUrls,
        String sTypename}) {
    this._id = id;
    this._name = name;
    this._following = following;
    this._headerUrl = headerUrl;
    this._headerUrls = headerUrls;
    this._sTypename = sTypename;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  bool get following => _following;
  set following(bool following) => _following = following;
  String get headerUrl => _headerUrl;
  set headerUrl(String headerUrl) => _headerUrl = headerUrl;
  Null get headerUrls => _headerUrls;
  set headerUrls(Null headerUrls) => _headerUrls = headerUrls;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  Author.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _following = json['following'];
    _headerUrl = json['headerUrl'];
    _headerUrls = json['headerUrls'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['following'] = this._following;
    data['headerUrl'] = this._headerUrl;
    data['headerUrls'] = this._headerUrls;
    data['__typename'] = this._sTypename;
    return data;
  }
}

class Tag {
  int _type;
  String _name;
  String _sTypename;

  Tag({int type, String name, String sTypename}) {
    this._type = type;
    this._name = name;
    this._sTypename = sTypename;
  }

  int get type => _type;
  set type(int type) => _type = type;
  String get name => _name;
  set name(String name) => _name = name;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  Tag.fromJson(Map<String, dynamic> json) {
    _type = json['type'];
    _name = json['name'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this._type;
    data['name'] = this._name;
    data['__typename'] = this._sTypename;
    return data;
  }
}

class Photo {
  String _id;
  int _duration;
  String _caption;
  String _likeCount;
  int _realLikeCount;
  String _coverUrl;
  Null _coverUrls;
  List<PhotoUrls> _photoUrls;
  String _photoUrl;
  bool _liked;
  int _timestamp;
  String _expTag;
  String _animatedCoverUrl;
  int _stereoType;
  double _videoRatio;
  String _sTypename;

  Photo(
      {String id,
        int duration,
        String caption,
        String likeCount,
        int realLikeCount,
        String coverUrl,
        Null coverUrls,
        List<PhotoUrls> photoUrls,
        String photoUrl,
        bool liked,
        int timestamp,
        String expTag,
        String animatedCoverUrl,
        int stereoType,
        double videoRatio,
        String sTypename}) {
    this._id = id;
    this._duration = duration;
    this._caption = caption;
    this._likeCount = likeCount;
    this._realLikeCount = realLikeCount;
    this._coverUrl = coverUrl;
    this._coverUrls = coverUrls;
    this._photoUrls = photoUrls;
    this._photoUrl = photoUrl;
    this._liked = liked;
    this._timestamp = timestamp;
    this._expTag = expTag;
    this._animatedCoverUrl = animatedCoverUrl;
    this._stereoType = stereoType;
    this._videoRatio = videoRatio;
    this._sTypename = sTypename;
  }

  String get id => _id;
  set id(String id) => _id = id;
  int get duration => _duration;
  set duration(int duration) => _duration = duration;
  String get caption => _caption;
  set caption(String caption) => _caption = caption;
  String get likeCount => _likeCount;
  set likeCount(String likeCount) => _likeCount = likeCount;
  int get realLikeCount => _realLikeCount;
  set realLikeCount(int realLikeCount) => _realLikeCount = realLikeCount;
  String get coverUrl => _coverUrl;
  set coverUrl(String coverUrl) => _coverUrl = coverUrl;
  Null get coverUrls => _coverUrls;
  set coverUrls(Null coverUrls) => _coverUrls = coverUrls;
  List<PhotoUrls> get photoUrls => _photoUrls;
  set photoUrls(List<PhotoUrls> photoUrls) => _photoUrls = photoUrls;
  String get photoUrl => _photoUrl;
  set photoUrl(String photoUrl) => _photoUrl = photoUrl;
  bool get liked => _liked;
  set liked(bool liked) => _liked = liked;
  int get timestamp => _timestamp;
  set timestamp(int timestamp) => _timestamp = timestamp;
  String get expTag => _expTag;
  set expTag(String expTag) => _expTag = expTag;
  String get animatedCoverUrl => _animatedCoverUrl;
  set animatedCoverUrl(String animatedCoverUrl) =>
      _animatedCoverUrl = animatedCoverUrl;
  int get stereoType => _stereoType;
  set stereoType(int stereoType) => _stereoType = stereoType;
  double get videoRatio => _videoRatio;
  set videoRatio(double videoRatio) => _videoRatio = videoRatio;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  Photo.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _duration = json['duration'];
    _caption = json['caption'];
    _likeCount = json['likeCount'];
    _realLikeCount = json['realLikeCount'];
    _coverUrl = json['coverUrl'];
    _coverUrls = json['coverUrls'];
    if (json['photoUrls'] != null) {
      _photoUrls = new List<PhotoUrls>();
      json['photoUrls'].forEach((v) {
        _photoUrls.add(new PhotoUrls.fromJson(v));
      });
    }
    _photoUrl = json['photoUrl'];
    _liked = json['liked'];
    _timestamp = json['timestamp'];
    _expTag = json['expTag'];
    _animatedCoverUrl = json['animatedCoverUrl'];
    _stereoType = json['stereoType'];
    _videoRatio = json['videoRatio'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['duration'] = this._duration;
    data['caption'] = this._caption;
    data['likeCount'] = this._likeCount;
    data['realLikeCount'] = this._realLikeCount;
    data['coverUrl'] = this._coverUrl;
    data['coverUrls'] = this._coverUrls;
    if (this._photoUrls != null) {
      data['photoUrls'] = this._photoUrls.map((v) => v.toJson()).toList();
    }
    data['photoUrl'] = this._photoUrl;
    data['liked'] = this._liked;
    data['timestamp'] = this._timestamp;
    data['expTag'] = this._expTag;
    data['animatedCoverUrl'] = this._animatedCoverUrl;
    data['stereoType'] = this._stereoType;
    data['videoRatio'] = this._videoRatio;
    data['__typename'] = this._sTypename;
    return data;
  }
}

class PhotoUrls {
  String _cdn;
  String _url;
  String _sTypename;

  PhotoUrls({String cdn, String url, String sTypename}) {
    this._cdn = cdn;
    this._url = url;
    this._sTypename = sTypename;
  }

  String get cdn => _cdn;
  set cdn(String cdn) => _cdn = cdn;
  String get url => _url;
  set url(String url) => _url = url;
  String get sTypename => _sTypename;
  set sTypename(String sTypename) => _sTypename = sTypename;

  PhotoUrls.fromJson(Map<String, dynamic> json) {
    _cdn = json['cdn'];
    _url = json['url'];
    _sTypename = json['__typename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cdn'] = this._cdn;
    data['url'] = this._url;
    data['__typename'] = this._sTypename;
    return data;
  }
}
