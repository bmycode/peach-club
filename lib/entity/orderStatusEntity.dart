class orderStatusEntity {
  int _code;
  String _error;
  List<Arr> _arr;

  orderStatusEntity({int code, String error, List<Arr> arr}) {
    this._code = code;
    this._error = error;
    this._arr = arr;
  }

  int get code => _code;
  set code(int code) => _code = code;
  String get error => _error;
  set error(String error) => _error = error;
  List<Arr> get arr => _arr;
  set arr(List<Arr> arr) => _arr = arr;

  orderStatusEntity.fromJson(Map<String, dynamic> json) {
    _code = json['code'];
    _error = json['error'];
    if (json['arr'] != null) {
      _arr = new List<Arr>();
      json['arr'].forEach((v) {
        _arr.add(new Arr.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this._code;
    data['error'] = this._error;
    if (this._arr != null) {
      data['arr'] = this._arr.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Arr {
  String _activateStatus;
  String _createdAt;
  String _objectId;
  String _orderNumber;
  String _superiorWeixin;
  String _updatedAt;
  String _userObjectId;
  String _vipLevel;

  Arr(
      {String activateStatus,
        String createdAt,
        String objectId,
        String orderNumber,
        String superiorWeixin,
        String updatedAt,
        String userObjectId,
        String vipLevel}) {
    this._activateStatus = activateStatus;
    this._createdAt = createdAt;
    this._objectId = objectId;
    this._orderNumber = orderNumber;
    this._superiorWeixin = superiorWeixin;
    this._updatedAt = updatedAt;
    this._userObjectId = userObjectId;
    this._vipLevel = vipLevel;
  }

  String get activateStatus => _activateStatus;
  set activateStatus(String activateStatus) => _activateStatus = activateStatus;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get objectId => _objectId;
  set objectId(String objectId) => _objectId = objectId;
  String get orderNumber => _orderNumber;
  set orderNumber(String orderNumber) => _orderNumber = orderNumber;
  String get superiorWeixin => _superiorWeixin;
  set superiorWeixin(String superiorWeixin) => _superiorWeixin = superiorWeixin;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get userObjectId => _userObjectId;
  set userObjectId(String userObjectId) => _userObjectId = userObjectId;
  String get vipLevel => _vipLevel;
  set vipLevel(String vipLevel) => _vipLevel = vipLevel;

  Arr.fromJson(Map<String, dynamic> json) {
    _activateStatus = json['activateStatus'];
    _createdAt = json['createdAt'];
    _objectId = json['objectId'];
    _orderNumber = json['orderNumber'];
    _superiorWeixin = json['superiorWeixin'];
    _updatedAt = json['updatedAt'];
    _userObjectId = json['userObjectId'];
    _vipLevel = json['vipLevel'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['activateStatus'] = this._activateStatus;
    data['createdAt'] = this._createdAt;
    data['objectId'] = this._objectId;
    data['orderNumber'] = this._orderNumber;
    data['superiorWeixin'] = this._superiorWeixin;
    data['updatedAt'] = this._updatedAt;
    data['userObjectId'] = this._userObjectId;
    data['vipLevel'] = this._vipLevel;
    return data;
  }
}