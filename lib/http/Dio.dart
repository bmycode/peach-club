import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';

/// 单例模式封装 dio
class HttpApi {
  Dio _Dio;
  static HttpApi _instance = HttpApi.instance();
  factory HttpApi() => _instance;

  HttpApi.instance() {
    if (null == _Dio) {
      BaseOptions options = new BaseOptions(
        baseUrl: Config.ApiUrl['PhotoBaseUrl'],
        connectTimeout: 30000,
        contentType: 'application/json; charset=utf-8',
        responseType: ResponseType.json,
      );

      _Dio = new Dio(options);
      _Dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
        // 请求拦截器
        return options;
      },onResponse: (Response response) {
        // 响应拦截器
        return response;
      },onError: (DioError e) {

        print("网络请求出现错误 ====== ${e.message}");
        // 错误拦截器
        return e;
      }));
    }
  }

  /// GET 请求
  Future get({@required String url, Options options, dynamic params}) async {
    SetBaseUrl(url);
    Response res = await _Dio.get(url, queryParameters: params, options: options);
    return res.data;
  }

  /// POST 请求
  Future post({@required String url, Options options, dynamic params}) async {
    SetBaseUrl(url);
    Response res = await _Dio.post(url, data: params, options: options);
    return res.data;
  }

  void SetBaseUrl(String url) {
    if (url == "/likeList") {
      _Dio.options.baseUrl = Config.ApiUrl['VideoBaseUrl'];
    } else {
      _Dio.options.baseUrl = Config.ApiUrl['PhotoBaseUrl'];
    }
  }

  /// 下载文件
  Future downloadFile(String urlPath, String savePath, { ProgressCallback onReceiveProgress }) async {
    Response response = await _Dio.download(
        urlPath,
        savePath,
        onReceiveProgress: onReceiveProgress,
        options: Options(sendTimeout: 25000, receiveTimeout: 25000));
    return response;
  }

}
