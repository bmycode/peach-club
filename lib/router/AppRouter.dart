import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/router/AppNode.dart';
import 'package:peach/config/_.dart';
import 'package:peach/views/User/AgentRegister.dart';

import 'package:peach/views/Welcome.dart';
import 'package:peach/views/Details/index.dart';
import 'package:peach/views/App.dart';

import 'package:peach/views/Me/Like.dart';
import 'package:peach/views/Me/Agreement.dart';
import 'package:peach/views/Me/Recently.dart';
import 'package:peach/views/Me/SingleGraph.dart';
import 'package:peach/views/Me/About.dart';
import 'package:peach/views/Me/Vip/Vip.dart';
import 'package:peach/views/Me/Vip/BuyStatus.dart';

import 'package:peach/views/Me/Agent.dart';
import 'package:peach/views/Me/Video/Video.dart';
// import 'package:peach/views/Me/Video/VideoDetails.dart';

import 'package:peach/views/Home/Edit.dart';
import 'package:peach/views/Home/Search.dart';

import 'package:peach/views/Model/ModelInfo.dart';
import 'package:peach/views/Mechanism/MechanismList.dart';

import 'package:peach/views/User/Login.dart';
import 'package:peach/views/User/Register.dart';
import 'package:peach/views/User/AgentRegister.dart';
/*
 * 路由配置
 */
class AppRouter extends MainModule {

  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter('/', child: (_, __) => Config.isFirstOpen ? Welcome() : Config.isLogin ? Login() : App(),
      transition: TransitionType.fadeIn,
    ),
    ModularRouter('/init', child: (_, __) => App(), transition: TransitionType.fadeIn,),
    ModularRouter('/like', child: (_, __) => Like(), transition: TransitionType.fadeIn,),
    ModularRouter('/edit', child: (_, __) => Edit(), transition: TransitionType.fadeIn,),
    ModularRouter('/agreement', child: (_, __) => Agreement(), transition: TransitionType.fadeIn,),
    ModularRouter('/about', child: (_, __) => About(), transition: TransitionType.fadeIn,),
    ModularRouter('/video', child: (_, __) => Video(), transition: TransitionType.fadeIn,),
    ModularRouter('/singlegraph', child: (_, __) => SingleGraph(), transition: TransitionType.fadeIn,),
    ModularRouter('/recently', child: (_, __) => Recently(), transition: TransitionType.fadeIn,),
    ModularRouter('/search', child: (_, __) => Search(), transition: TransitionType.fadeIn,),
    ModularRouter('/login', child: (_, __) => Login(), transition: TransitionType.fadeIn,),
    ModularRouter('/reg', child: (_, __) => Register(), transition: TransitionType.fadeIn,),
    ModularRouter('/vip', child: (_, __) => Vip(), transition: TransitionType.fadeIn,),
    ModularRouter('/status', child: (_, __) => BuyStatus(), transition: TransitionType.fadeIn,),
    ModularRouter('/agent', child: (_, __) => Agent(), transition: TransitionType.fadeIn,),
    ModularRouter('/agentReg', child: (_, __) => AgentRegister(), transition: TransitionType.fadeIn,),
    // ModularRouter('/videoDetails/:title/:mp4/:m3u8/:gif', child: (_, args) => VideoDetails(
    //   title: args.params['title'],
    //   mp4: args.params['mp4'],
    //   m3u8: args.params['m3u8'],
    //   gif: args.params['gif'],
    // ), transition: TransitionType.fadeIn,),

    ModularRouter('/modelInfo/:id/:title', child: (_, args) => ModelInfo(
      id: args.params['id'],
      title: args.params['title'],
    ), transition: TransitionType.fadeIn,),

    ModularRouter('/mechanismList/:id/:title/:type', child: (_, args) => MechanismList(
      id: args.params['id'],
      title: args.params['title'],
      type: args.params['type']
    ), transition: TransitionType.fadeIn,),

    ModularRouter('/details/:id', child: (_, args) => DetailsIndex(
        id: args.params['id']
    ), transition: TransitionType.fadeIn,
    ),
  ];

  @override
  Widget get bootstrap => AppNode();
}