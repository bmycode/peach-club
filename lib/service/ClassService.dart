import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/db/SQLite.dart';
import 'package:peach/service/_.dart';

class ClassService {

  static String tableNamePrincipal = "Principal"; // 默认菜单
  static String tableNameAdditional = "Additional";// 可选菜单

  /// 向指定表插入List数据
  static Future<void> addList(String tableName, List<Principal> res) async {
    List<Map<String, dynamic>> data = await ClassService.findAll(tableName);
    // 如果表中没有数据，执行插入
    if (data.isEmpty) {
      var _db = await SQLite().db;
      res.forEach((e) async {
        await _db.insert(tableName, e.toJson());
      });
    }
  }

  /// 查询指定表中菜单数据
  static Future<List<Map<String, dynamic>>> findAll(String tableName) async {
    var _db = await await SQLite().db;
    List<Map<String, dynamic>> result = await _db.query(tableName, orderBy: "id DESC");
    return result.isNotEmpty
          ? result
          : [];
  }

  /// 添加菜单到指定表
  static Future<int> add({ String tableName , Principal item }) async {
    var _db = await SQLite().db;
    return await _db.insert(tableName, item.toJson());
  }

  /// 从指定表删除菜单
  static Future<int> delete({ String tableName , Principal item }) async {
    var _db = await await SQLite().db;
    return await _db.delete(
        tableName,
        where: 'id = ?',
        whereArgs: [item.id]
    );
  }

}
