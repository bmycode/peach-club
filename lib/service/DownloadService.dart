import 'package:dio/dio.dart';
import 'package:peach/http/Dio.dart';

class DownloadService {

  /// 下载图片
  static Future downImage({ String url, Map<String, dynamic> params}) async {
    return await HttpApi().get(
      url: url,
      params: params,
      options: Options(responseType: ResponseType.bytes)
    );
  }
}
