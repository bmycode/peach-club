import 'package:peach/config/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/ClassService.dart';

class HomeService {

  /// 获取分类菜单
  static Future<ClassResult> GetClassData({ Map<String, dynamic> params }) async {
    List<Map<String, dynamic>> a = await ClassService.findAll("Principal");

    // 如果数据库有数据，那么直接返回数据库的数据
    if (a.isNotEmpty) {
      print("如果数据库有分类的数据，那么直接");
      List<Map<String, dynamic>> b = await ClassService.findAll("Additional");
      return ClassResult.fromJson({
        'principal': a,
        'additional': b,
      });
    // 没有数据调接口请求新的分类
    } else {
      print("如果数据库没有分类的数据，那么掉接口");
      var res = await HttpApi().get(
          url: Config.ApiUrl['ApiList']['class'],
          params: params
      );
      var resJson = ClassEntity.fromJson(res).result;
      resJson.principal.forEach((v) {
        print("principal =============== ${v.id}");
      });

      resJson.additional.forEach((v) {
        print("additional ++++++++++++++++++ ${v.id}");
      });
      await ClassService.addList("Principal",resJson.principal);
      await ClassService.addList("Additional",resJson.additional);
      return resJson;
    }
  }

  /// 获取分类下的数据列表
  static Future<AgentClassListData> GetClassListData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentClassList'],
        params: params
    );
    return AgentClassListEntity.fromJson(res).data;
  }

  /// 获取详情页数据
  static Future<AgentInfo> GetInfoData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentInfo'],
        params: params
    );
    return AgentInfoEntity.fromJson(res).data;
  }

  /// 获取首页数据
  static Future<Result> GetHomeData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
      url: Config.ApiUrl['ApiList']['index'],
      params: params
    );
    return HomeEntity.fromJson(res).result;
  }

  /// App更新接口
  static Future<UpdateVersionEntity> update({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
        url: Config.ApiUrl['ApiList']['update'],
        params: params
    );
    return UpdateVersionEntity.fromJson(res);
  }


  static Future<AgentClassListData> GetRecently({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentRecently'],
        params: params
    );
    return AgentClassListEntity.fromJson(res).data;
  }

  /// 获取搜索的数据
  static Future<List<ItemList>> GetSearchData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentSearch'],
        params: params
    );
    return SearchEntity.fromJson(res).data;
  }

  // 获取随机数据
  static Future<AgentClassListData> GetAgentRandom({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentRandom'],
        params: params
    );
    return AgentClassListEntity.fromJson(res).data;
  }


}
