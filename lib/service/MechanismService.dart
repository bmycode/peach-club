import 'package:peach/config/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/entity/_.dart';

class MechanismService {

  /// 获取分类列表的数据
  static Future<List<MechanismResult>> GetMechanismData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
        url: Config.ApiUrl['ApiList']['mechanism'],
        params: params
    );
    return MechanismEntity.fromJson(res).result;
  }

  /// 机构列表点击进入详情
  static Future<AgentClassListData> GetmechanismList({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentMechanismList'],
        params: params
    );
    return AgentClassListEntity.fromJson(res).data;
  }

}
