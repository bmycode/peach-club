import 'package:peach/config/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/entity/_.dart';

class ModelService {

  /// 模特列表
  static Future<ModelResult> GetModelData({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
        url: Config.ApiUrl['ApiList']['allModel'],
        params: params
    );
    return ModelEntity.fromJson(res).result;
  }


  /// 进入模特主页
  static Future<ModelInfoResult> GetModelInfo({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
        url: Config.ApiUrl['ApiList']['modelList'],
        params: params
    );
    print(" ===== > ${res}");
    return ModelInfoEntity.fromJson(res).result;
  }

}
