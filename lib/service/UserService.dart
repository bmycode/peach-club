import 'package:peach/config/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/entity/_.dart';

class UserService {

  /// 用户登录接口
  static Future<LoginEntity> userLogin({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['login'],
        params: params
    );
    return LoginEntity.fromJson(res);
  }

  /// 用户注册接口
  static Future<RegisterEntity> userRegister({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['register'],
        params: params
    );
    return RegisterEntity.fromJson(res);
  }

  /// 代理用户注册
  static Future<RegisterEntity> agentUserRegister({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['agentreg'],
        params: params
    );
    return RegisterEntity.fromJson(res);
  }

  /// 免费试看的次数限制
  static Future<FreeUseEntity> freeUse({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['freeUse'],
        params: params
    );
    return FreeUseEntity.fromJson(res);
  }

  /// 办理会员的时候的 下单接口
  static Future<RegisterEntity> placeOrder({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['place'],
        params: params
    );
    return RegisterEntity.fromJson(res);
  }

  /// 办理会员的时候的 下单接口
  static Future<orderStatusEntity> orderStatus({ Map<String, dynamic> params }) async {
    var res = await HttpApi().post(
        url: Config.ApiUrl['ApiList']['orderStatus'],
        params: params
    );
    return orderStatusEntity.fromJson(res);
  }
}
