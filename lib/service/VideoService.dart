import 'package:dio/dio.dart';
import 'package:peach/config/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/entity/_.dart';

class VideoService {

  /// 视频列表
  static Future<VideoListEntity> GetVideoList({ Map<String, dynamic> params }) async {
    var res = await HttpApi().get(
        url: Config.ApiUrl['ApiList']['videoType'],
        params: params
    );
    return VideoListEntity.fromJson(res);
  }

}
