import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_update_dialog/update_dialog.dart';
import 'package:install_plugin/install_plugin.dart';
import 'package:path_provider/path_provider.dart';
import 'package:peach/R.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/http/Dio.dart';
import 'package:peach/service/_.dart';

/*
 * 检测新版本
 */
class CheckUpdate {
  // 保存升级弹窗
  UpdateDialog _dialog;
  double _progress = 0.0;
  File _apkFile;

  Future<bool> check(BuildContext context) async {
    // 检查版本
    UpdateVersionEntity res = await HomeService.update(params: {});

    // 如果需要升级
    if (int.parse(Config.packageInfo.buildNumber) < res.versionCode) {
      if (_dialog != null && _dialog.isShowing()) {
        return false;
      }
      // 升级的内容信息
      String updateContent = "";
      res.updateMsg.forEach((v) => updateContent = updateContent + "${v}\n");

      // 显示弹窗
      _dialog = UpdateDialog.showUpdate(context,
          title: "发现新版本v${res.version}，请升级！",
          updateContent: updateContent,
          width: 250,
          topImage: Image.asset(R.libStaticImgUpdateBgAppTopPng),
          themeColor: ColorConfig.ThemeColor,
          updateButtonText: '升级',
          isForce: true,
          onUpdate: () async {
            // 如果是android 那么下载app
            if (Config.isAndroid) {
              // 获取临时下载地址
              _apkFile = await getApkFileByUpdateEntity(res);
              await onUpdate(res);
            // 如果是ios 那么打开appstore
            } else {
              InstallPlugin.gotoAppStore(Config.iosDownApkUrl);
            }
          }
      );
      return true;
    } else {
      return false;
    }
  }

  // 下载文件，更新进度条
  Future<void> onUpdate(UpdateVersionEntity res) async {
    await HttpApi().downloadFile(
        res.downUrl, _apkFile.path,
        onReceiveProgress: (int count, int total) {
          _progress = count.toDouble() / total;
          if (_progress <= 1.0001) {
            _dialog.update(_progress);
          }
        }).then((value) {
          _dialog.dismiss();
          installAPP();
        }).catchError((value) {
          _dialog.dismiss();
        });
  }

  /// android 安装app
  void installAPP() async {
    String packageName = Config.packageInfo.packageName;
    InstallPlugin.installApk(_apkFile.path, packageName);
  }

  /// 根据更新信息获取apk安装文件
  Future<File> getApkFileByUpdateEntity(UpdateVersionEntity res) async {
    // 获取app文件名
    String appName = getApkNameByDownloadUrl(res.downUrl);
    // 获取下载的缓存路径
    String dirPath = await getDownloadDirPath();
    return File("$dirPath/${res.versionCode}/$appName");
  }

  ///获取下载缓存路径
  Future<String> getDownloadDirPath() async {
    Directory directory = Config.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }

  ///根据下载地址获取文件名
  String getApkNameByDownloadUrl(String downloadUrl) {
    if (downloadUrl.isEmpty) {
      return "temp_${currentTimeMillis()}.apk";
    } else {
      String appName = downloadUrl.substring(downloadUrl.lastIndexOf("/") + 1);
      if (!appName.endsWith(".apk")) {
        appName = "temp_${currentTimeMillis()}.apk";
      }
      return appName;
    }
  }

  /// 返回随机文件名
  int currentTimeMillis() {
    return DateTime.now().millisecondsSinceEpoch;
  }
}
