import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/Colors.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import '_.dart';

/*
 * 核心的utils，主要封装UI相关的操作
 */
class Core {

  static FreeUseEntity freeUseStatus;

  /// 修改状态栏颜色
  static void SetStatusThemeColor({Color color : ColorConfig.ThemeColor}) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: ColorConfig.ThemeColor
    ));
  }

  static void deleteDialog(BuildContext context, Function callback,{ String content: "确认删除该收藏吗？" }) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('提示'),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  child: Text('取消',style: TextStyle(color: ColorConfig.NoActiveColor),),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
              ),
              FlatButton(
                  child: Text('确认',style: TextStyle(color: ColorConfig.ThemeColor),),
                  onPressed: () async {
                    callback();
                    Navigator.of(context).pop();
                  }
              ),
            ],
          );
        });
  }

  /// 调用接口，减少免费试看的次数
  static Future<FreeUseEntity> freeUse(BuildContext context, Function vipCallBack) async {
    freeUseStatus = await UserService.freeUse(params: { 'objectId':  Tool.userInfo()['objectId'] });
    // 如果已经没有免费试看的次数了，弹窗提醒
    if (freeUseStatus.freeNumber == 0) {
      await showDialog(
          context: context,
          builder: (context) {
            return WillPopScope(
              onWillPop: () async => false,
              child: AlertDialog(
                title: Text('温馨提示'),
                content: Text("免费体验到期，请开通VIP，享受更多特权和功能！"),
                actions: <Widget>[
                  FlatButton(
                      child: Text('取消',style: TextStyle(color: ColorConfig.NoActiveColor),),
                      onPressed: () {
                        Modular.to.pop();
                        Modular.to.pop();
                      }
                  ),
                  FlatButton(
                      child: Text('开通VIP',style: TextStyle(color: ColorConfig.ThemeColor),),
                      onPressed: () async {
                        Modular.to.pushNamed('/vip');
                      }
                  ),
                ],
              ),
            );
          });
    } else {
      vipCallBack();
    }
  }

  // -1 表示已经开通了vip
  static bool isVip(BuildContext context) {
    print("freeUseStatus.freeNumber ======= ${freeUseStatus.freeNumber}");
    if (freeUseStatus.freeNumber == -1) {
      return true;
    } else {
      return false;
    }
  }

  /// 复制文字到剪切板
  static void copy (String message) {
    Clipboard.setData(ClipboardData(text: message));
  }


}