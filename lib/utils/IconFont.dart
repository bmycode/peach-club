import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';

/*
 * IconFont 字体图标
 */
class IconFont {

  static Icon LogoIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe61c, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon ModelIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe61f, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon MeIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe611, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon MechanismIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe61a, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon HomeIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe617, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon HistoryIcon ({double size = 24.0, Color color = ColorConfig.WhiteBackColor}) {
    return Icon(
      const IconData(0xe600, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon VipIcon ({double size = 24.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe6ba, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon DaLiIcon ({double size = 24.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe618, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon UpdateIcon ({double size = 24.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe63c, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon ClearIcon ({double size = 22.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe616, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon XieYiIcon ({double size = 22.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe6b2, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon AboutIcon ({double size = 22.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe637, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon NewsIcon ({double size = 22.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe6c0, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon VideoIcon ({double size = 22.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe60c, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon PictureIcon ({double size = 20.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe61e, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon BuyIcon ({double size = 20.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe73b, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon StatusIcon ({double size = 20.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe655, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon OrderIcon ({double size = 20.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe620, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }

  static Icon WeiXinIcon ({double size = 20.0, Color color = ColorConfig.TextColor}) {
    return Icon(
      const IconData(0xe64f, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
  }
}