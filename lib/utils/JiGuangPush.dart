
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:peach/config/_.dart';

/*
 * 极光消息推送
 */
class JiGuangPush {
  JPush _jpush;
  static JiGuangPush _instance = JiGuangPush.instance();
  factory JiGuangPush() => _instance;

  JiGuangPush.instance() {
    if (null == _jpush) {
      _jpush = new JPush();
      addEventHandler();
      _jpush.setup(
        appKey: Config.JgConfig['appKey'],
        channel: Config.JgConfig['channel'],
        production: Config.JgConfig['production'],
        debug: Config.JgConfig['debug'],
      );
    }
  }

  addEventHandler() {
    _jpush.addEventHandler(
      // 接收通知回调方法。
      onReceiveNotification: (Map<String, dynamic> message) async {
        print("收到的消息为：: $message");
      },
      // 点击通知回调方法。
      onOpenNotification: (Map<String, dynamic> message) async {
        print("消息被点击了: $message");
      },
      // 接收自定义消息回调方法。
      onReceiveMessage: (Map<String, dynamic> message) async {
        print("flutter onReceiveMessage: $message");
      },
    );
  }
}