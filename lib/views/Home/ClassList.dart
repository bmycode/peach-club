import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:toast/toast.dart';

/*
 * 横向菜单除首页外的布局
 */
class ClassList extends StatefulWidget {
  int id;
  int totalPage = 0;
  List<ItemList> result = [];

  EasyRefreshController _controller; // EasyRefresh控制器

  ClassList({Key key, @required this.id }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ClassList();
}

class _ClassList extends State<ClassList> with AutomaticKeepAliveClientMixin {

  int page = 1;

  @override
  void initState() {
    super.initState();
    widget._controller = EasyRefreshController();
    new Future.delayed(Duration(milliseconds: 500), () async {
      getClassListData();
    });
  }

  /// 请求数据，类型是根据ajax生成的数据实体类
  Future<AgentClassListData> getClassListData() async {
    AgentClassListData res = await HomeService.GetClassListData(params: {
      'id': widget.id,
      'page': page
    });
    setState(() {
      widget.result.addAll(res.list);
      widget.totalPage = int.parse(res.total);
      page = int.parse(res.page);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return EasyRefresh(
      header: BezierCircleHeader(backgroundColor: ColorConfig.ThemeColor),
      footer: BezierBounceFooter(backgroundColor: ColorConfig.ThemeColor),
      onRefresh: () async {
        // widget.page  = 0;
        // widget.result = [];
        // await getClassListData();
      },
      onLoad: () async {
        page += 1;
        if (page > widget.totalPage) {
          Toast.show("没有数据", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        } else {
          await getClassListData();
          widget._controller.finishRefresh();
        }
      },
      child: ItemWidget(
          data: widget.result,
          title: '',
          longPress: () {}
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
