import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';
import 'package:peach/service/_.dart';

/*
 * 首页的布局
 */
class DefaultLayout extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Default();

}

class _Default extends State<DefaultLayout> with AutomaticKeepAliveClientMixin<DefaultLayout> {

  @override
  void initState() {
    super.initState();
  }

  /// 请求数据，被异步数据组件 FutureBuilder 调用
  Future<Result> GetHomeData() async {
    return await HomeService.GetHomeData(params: {});
  }

  /// 首页的列表
  Widget DeaultPageList(Result result) {
    var Models = Tool.splitList(result.model.res, 4);
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          SizedBox(height: Screen.setHeight(18),),
          // SwiperWidget(
          //     autoplayDelay: 4000,
          //     autoplay: false,
          //     child: [
          //       ModelWidget(data: Models[0]),
          //       ModelWidget(data: Models[1]),
          //     ]
          // ),
          OldItemWidget(icon: Icons.new_releases_sharp, title: '最新收录', data: result.newest.data),
          OldItemWidget(icon: Icons.thumb_up_alt,title: '推荐写真', data: result.recommended.data,)
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return FutureBuilderWidget<Result>(
      future: GetHomeData,
      success: DeaultPageList,
    );
  }

  @override
  bool get wantKeepAlive => true;

}