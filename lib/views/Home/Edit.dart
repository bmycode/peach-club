import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:toast/toast.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/event/_.dart';

/*
 * 菜单编辑页面
 */
class Edit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Edit();
}

class _Edit extends State<Edit> {

  // 已选菜单
  List<Principal> principal = [];
  // 未选菜单
  List<Principal> additional = [];
  bool isEdit = false;

  @override
  void initState() {
    GetList();
  }

  void GetList () {
    new Future.delayed(Duration(milliseconds: 280),() async {
      var a = await ClassService.findAll("Principal");
      var b = await ClassService.findAll("Additional");

      setState(() {
        principal = a.map((e) => Principal.fromJson(e) ).toList();
        additional = b.map((e)=> Principal.fromJson(e)).toList();
      });
    });
  }

  // 已选菜单 被点击的时候
  principalClick(Principal item) {
    // 删除原本的数据
    principal = principal.where((v) => v.id != item.id).toList();

    var isHave = additional.where((v) => v.id == item.id).toList();
    if (isHave.length == 0) {
      // 向 未选菜单 添加数据
      additional.addAll([item]);
      ClassService.add(tableName: 'Additional', item: item);
    }

    isEdit = true;
    setState(() {});
    ClassService.delete(tableName: 'Principal', item: item);
  }

  // 未选菜单 被点击的时候
  additionalClick(Principal item) {
    // 删除原本的数据
    additional = additional.where((v) => v.id != item.id).toList();
    // 向 已选菜单 添加数据
    principal.addAll([item]);
    isEdit = true;
    setState(() {});
    ClassService.delete(tableName: 'Additional', item: item);
    ClassService.add(tableName: 'Principal', item: item);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text("分类编辑"),
            backgroundColor: ColorConfig.ThemeColor,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back),
              onPressed: () {
                Config.eventBus.fire(ClassEditEvent(isEdit));
                Modular.to.pop();
              },
            ),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            padding: EdgeInsets.only(top: 15,right: 15,bottom: 0,left: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  padding: EdgeInsets.all(8),
                  color: Color(0XFFe1e1e3),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("注意",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                      SizedBox(height: Screen.setHeight(10),),
                      Text("1：如果编辑了分类，首页之前的浏览进度将丢失。",style: TextStyle(),),
                      SizedBox(height: Screen.setHeight(5),),
                      Text("2：如果没有编辑分类，那么返回首页，之前的浏览进度都还会在。",style: TextStyle(),),
                      SizedBox(height: Screen.setHeight(5),),
                      Text("3：如果你删除【已选菜单】的所有分类，那么将默认从服务器请求最新菜单",style: TextStyle(),),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("已选菜单",style: TextStyle(fontSize: Screen.setFontSize(16),fontWeight: FontWeight.w400),),
                      SizedBox(height: Screen.setHeight(5),),
                      Wrap(
                        spacing: Screen.setFontSize(13),
                        children: principal.map((e) => RawChip(
                          label: Text(e.title,style: TextStyle(fontSize: 12,color: ColorConfig.WhiteBackColor)),
                          padding: EdgeInsets.all(1),
                          backgroundColor: ColorConfig.ThemeOtherColor,
                          selectedColor: ColorConfig.ThemeColor,
                          checkmarkColor: Colors.white,
                          onPressed: () {
                            principalClick(e);
                          },
                        )).toList(),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: Screen.setFontSize(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("未选菜单",style: TextStyle(fontSize: Screen.setFontSize(16),fontWeight: FontWeight.w400),),
                      SizedBox(height: Screen.setHeight(5),),
                      Wrap(
                        spacing: Screen.setFontSize(18),
                        children: additional.map((e) => RawChip(
                          label: Text(e.title,style: TextStyle(fontSize: Screen.setFontSize(12),color: ColorConfig.WhiteBackColor)),
                          padding: EdgeInsets.all(1),
                          backgroundColor: ColorConfig.ThemeOtherColor,
                          selectedColor: ColorConfig.ThemeColor,
                          checkmarkColor: Colors.white,
                          onPressed: () {
                            additionalClick(e);
                          },
                        )).toList(),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        onWillPop: () async {
          Config.eventBus.fire(ClassEditEvent(isEdit));
          return true;
        });
  }
}