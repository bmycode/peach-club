import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';


/*
 * 随机展示
 */
class Random extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _Random();
}

class _Random extends State<Random> with AutomaticKeepAliveClientMixin{

  AgentClassListData result;
  List<ItemList> list = [];
  EasyRefreshController _controller; // EasyRefresh控制器


  @override
  void initState() {
    random();
  }

  Future<AgentClassListData> random() async {
    result = await HomeService.GetAgentRandom(params: {});
    setState(() {
      list.addAll(result.list);
    });
  }

  void Refresh() {
    list = [];
    setState(() {});
    random();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: EasyRefresh(
        header: BezierCircleHeader(backgroundColor: ColorConfig.ThemeColor),
        footer: BezierBounceFooter(backgroundColor: ColorConfig.ThemeColor),
        onRefresh: () async {
          Refresh();
        },
        onLoad: () async {
          random();
        },
        child: ItemWidget(
            data: list,
            title: '',
            longPress: () {}
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorConfig.ThemeColor,
        child: Icon(Icons.refresh,color: ColorConfig.WhiteBackColor,),
        onPressed: () {
          Refresh();
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}