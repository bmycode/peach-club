import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peach/R.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("关于我们"),
          backgroundColor: ColorConfig.ThemeColor,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(top: 110),
            padding: EdgeInsets.all(15),
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  R.libStaticImgLogoPng,
                  width: Screen.setWidth(70),
                  height: Screen.setWidth(70),
                ),
                SizedBox(height: 10,),
                Text(Config.appName, style: TextStyle(fontSize: 17,fontWeight: FontWeight.w400),),
                SizedBox(height: 6,),
                Text("十分专业的模特写真App，倾心为您提供国内外共计约198万张的写真图，满足您各种的口味和喜好是我们最初衷的动力！",
                  style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),textAlign: TextAlign.center),

                // Container(
                //   margin: EdgeInsets.only(top: 30),
                //   child: ButtonBar(
                //     alignment: MainAxisAlignment.center,
                //     mainAxisSize: MainAxisSize.max,
                //     children: [
                //       MaterialButton(
                //         color: ColorConfig.ThemeColor,
                //         textColor: Colors.white,
                //         onPressed: () async {
                //           await launch("tel:15155145354");
                //         },
                //         child: Row(
                //           children: [
                //             Icon(Icons.phone_callback,size: 14,),
                //             SizedBox(width: 2,),
                //             Text("电话联系",style: TextStyle(fontSize: 16))
                //           ],
                //         ),
                //       ),
                //       SizedBox(width: 10,),
                //       MaterialButton(
                //         color: ColorConfig.ThemeColor,
                //         textColor: Colors.white,
                //         onPressed: () async {
                //           await launch("sms:15155145354");
                //         },
                //         child: Row(
                //           children: [
                //             Icon(Icons.sms_outlined,size: 14,),
                //             SizedBox(width: 2,),
                //             Text("短信联系",style: TextStyle(fontSize: 16))
                //           ],
                //         ),
                //       ),
                //     ],
                //   ),
                // )
              ],
            ),
          ),
        )
    );
  }

}