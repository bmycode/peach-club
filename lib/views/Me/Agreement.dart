import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';

class Agreement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("用户协议"),
        backgroundColor: ColorConfig.ThemeColor,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        child: Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("用户须知",style: TextStyle(fontSize: 17,fontWeight: FontWeight.w700),),
              SizedBox(height: 15,),
              Text("1：图片和视频都来源于互联网，如果存在版权问题和纠纷，请通过邮箱（codehelp123456@protonmail.com）联系，我们也会停止相应资源的展示下载等一切行为！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("2：使用过程有任何的问题可以及时给予我们反馈。",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("3：本App内部分的内容少儿不宜，如果您未满18岁，请立即停止使用！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("4：如果您想成为代理赚取更多的收益，请加客服微信获取代理码，即可成为代理",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),

              SizedBox(height: 15,),
              Text("产品特点",style: TextStyle(fontSize: 17,fontWeight: FontWeight.w700),),
              SizedBox(height: 15,),
              Text("1：海量的超高清图集，图片画质超清晰、全部超大尺寸或原尺寸的图片。目前已经收录35804套图集，共计：197.4394万张高清图片，每周新增约100套+图集！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("2：在线浏览超高品质的图集，超一流的用户体验！一个帐号，电脑、手机、平板任性浏览！还可以收藏自己喜欢的图集，方便下次仔细欣赏。当然您也可以一键下载图集！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("3：全网最专业的图集收集整理网站！中国的、日韩的、欧美的、泰国的，应有尽有。全部都是我们精心整理的，按人物、机构、标签的精准确分类！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
              Text("4：特意开发的巧遇功能，随机从我们精心筛选的数千套图集里推送给你直接浏览体验。最贴心、好用、好看的功能！！",
                style: TextStyle(fontSize: 15,color: ColorConfig.TextColor),),
              SizedBox(height: 10,),
            ],
          ),
        ),
      )
    );
  }

}