import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class Like extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Like();
}

class _Like extends State<Like> {

  List<LikeEntity> LikeList = [];
  String initialValue = "default";
  final SlidableController slidableController = SlidableController();

  @override
  void initState() {
    getLikeList();
  }

  /// TODO 解决思路，再存数据的时候使用 HomeEntityData
  void getLikeList() async {
    var likeData = await LikeService.findAll();
    setState(() => LikeList = likeData);
  }

  Future<void> deleteAlertDialog(int id) async {
    await Core.deleteDialog(context, () async {
      await LikeService.delete(id);
      getLikeList();
    });
  }

  Widget UseLayout() {
    if (LikeList.isNotEmpty) {
      switch(initialValue) {
        case 'default':
          return OldItemWidget(
            data: LikeList,
            longPress: (int id) {
              deleteAlertDialog(id);
            },
          );
          break;
        case 'column':
          return Container(
            padding: EdgeInsets.only(left: 10,right: 10, top: 5),
            alignment: Alignment.centerLeft,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: LikeList.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext contenx,int index) {
                  return Slidable(
                    key: Key(index.toString()),
                    controller: slidableController,
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.25,
                    child: InkWell(
                      onLongPress: () => deleteAlertDialog(LikeList[index].id),
                      onTap: () => Modular.to.pushNamed("/details/${LikeList[index].id}"),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.network(
                            LikeList[index].preview,
                            width: Screen.setWidth(120),
                            height: Screen.setHeight(80),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 5,),
                                Text(LikeList[index].title, style: TextStyle(color: ColorConfig.TitleColor,fontSize: 15),),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Wrap(
                                    children: [
                                      Container(margin: EdgeInsets.only(right: 5),child: Text("模特：${LikeList[index].user}",style: TextStyle(color: ColorConfig.TextColor,fontSize: 13)),),
                                      Container(margin: EdgeInsets.only(right: 5),child: Text("来源：${LikeList[index].source}",style: TextStyle(color: ColorConfig.TextColor,fontSize: 13)),),
                                      Container(margin: EdgeInsets.only(right: 5),child: Text("图片：${LikeList[index].totals}P",style: TextStyle(color: ColorConfig.TextColor,fontSize: 13)),),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    secondaryActions: [//右侧按钮列表
                      IconSlideAction(
                        caption: '删除',
                        color: ColorConfig.ThemeColor,
                        foregroundColor: ColorConfig.WhiteBackColor,
                        icon: Icons.delete,
                        closeOnTap: false,
                        onTap: () {
                          Slidable.of(context)?.close();
                          deleteAlertDialog(LikeList[index].id);
                        },
                      ),
                    ],
                  );
                }
            ),
          );
          break;
        case 'big':
          return Container(
            alignment: Alignment.centerLeft,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: LikeList.length,
              physics: BouncingScrollPhysics(),
              itemBuilder: (BuildContext contenx,int index) {
                return InkWell(
                  onLongPress: () => deleteAlertDialog(LikeList[index].id),
                  onTap: () => Modular.to.pushNamed("/details/${LikeList[index].id}"),
                  child: Column(
                    children: [
                      Image.network(
                        LikeList[index].preview,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                    ],
                  ),
                );
              },
            ),
          );
          break;
      }
    } else {
      return Container(
        height: Screen.setHeight(300),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.error_outline_outlined,size: 50,color: ColorConfig.ThemeColor,),
            SizedBox(height: 10,),
            Text("暂无收藏", style: TextStyle(color: ColorConfig.TextColor))
          ],
        ),
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConfig.ThemeColor,
        title: Text("我的收藏"),
        actions: [
          Row(
            children: [
              Text(initialValue == "default" ? '默认布局' : initialValue == "column" ? "多列布局" : "大图布局"),
              new PopupMenuButton(
                initialValue: initialValue,
                itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
                  PopupMenuItem(
                      value: 'default',
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Icon(Icons.grid_view, color: ColorConfig.ThemeColor),
                          new Text("默认布局"),
                        ],
                      )
                  ),
                  PopupMenuItem(
                      value: 'column',
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Icon(Icons.table_rows_outlined, color: ColorConfig.ThemeColor),
                          new Text("多列布局"),
                        ],
                      )
                  ),
                  PopupMenuItem(
                      value: 'big',
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Icon(Icons.image_outlined, color: ColorConfig.ThemeColor),
                          new Text("大图模式"),
                        ],
                      )
                  )
                ],
                onSelected: (String action) {
                  print("action ===== ${action}");
                  setState(() {
                    initialValue = action;
                  });
                },
              )
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        child: UseLayout(),
      )
    );
  }


}