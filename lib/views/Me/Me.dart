import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/R.dart';
import 'package:share_extend/share_extend.dart';
import 'package:toast/toast.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';

class Me extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Me();
}

class _Me extends State<Me> {
  String cacheSize = "0.00B";

  @override
  void initState() {
    ComputeCache();
  }

  Future<void> ComputeCache() async {
    String size = await ClearCache.loadCache();
    setState(() {
      cacheSize = size;
    });
  }

  /// 分享给好友
  shareText() async {
    ShareExtend.share(
      "[有人@你]\n您的朋友给你分享了一个成人写真App。\n点击查看：${Config.shareIpUrl}?objectId=${Tool.userInfo()['objectId']}",
      "text",
      sharePanelTitle: "111",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        elevation: 0.0,
        backgroundColor: ColorConfig.ThemeColor,
      ),
      body: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Positioned(
            width: Screen.width(context),
            left: 0,
            top: 0,
            child: Container(
              padding: EdgeInsets.only(top: Screen.setHeight(40)),
              alignment: Alignment.topCenter,
              height: Screen.setHeight(150),
              color: ColorConfig.ThemeColor,
              child: Tool.userInfo()['userType'] == "0"
                      ? Text("我 的", style: TextStyle(fontSize: Screen.setFontSize(18),color: ColorConfig.WhiteBackColor),)
                      : Text("欢 迎 代 理", style: TextStyle(fontSize: Screen.setFontSize(18),color: ColorConfig.WhiteBackColor),),
            ),
          ),

          Positioned(
            width: Screen.width(context) / 1.1,
            top: Screen.setHeight(80),
            child: Container(
              height:  Tool.userInfo()['userType'] == "1" ? Screen.setHeight(100) : Screen.setHeight(80),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(1.0, 10.0), //阴影xy轴偏移量
                        blurRadius: 15.0, //阴影模糊程度
                        spreadRadius: 1.0 //阴影扩散程度
                    )
                  ]),
              padding: EdgeInsets.only(left: Screen.setFontSize(15), right: Screen.setFontSize(15),top: Screen.setFontSize(10)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      ClipOval(
                        child: Image.asset(
                          R.libStaticImgLogoPng,
                          width: Screen.setWidth(60),
                          height: Screen.setWidth(60),
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(width: Screen.setWidth(10),),
                      Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(Tool.userInfo()['username'],style: TextStyle(fontSize: Screen.setFontSize(18),color: ColorConfig.TitleColor),),
                              Text("注册时间: ${Tool.userInfo()['createdAt']}",style: TextStyle(fontSize: Screen.setFontSize(12),color: ColorConfig.NoActiveColor),),
                            ],
                          )
                      )
                    ],
                  ),
                  Tool.userInfo()['userType'] == "1"
                      ? Column(
                        children: [
                          CellWidget(
                            leftIcon: IconFont.DaLiIcon(),
                            leftText: Text("代理操作教程", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                            rightText: Text("必看!",style: TextStyle(fontSize: Screen.setFontSize(12),color: Color.fromARGB(87, 0, 0, 0))),
                            onTap: () {
                              Modular.to.pushNamed('/agent');
                            },
                          ),
                        ],
                      )
                      : Container(),
                ],
              ),
            ),
          ),

          Positioned(
            left: 0,
            right: 0,
            top: Tool.userInfo()['userType'] == "1" ? Screen.setHeight(180) : Screen.setHeight(160),
            bottom: 0,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    height: 15,
                  ),
                  CellWidget(
                    leftIcon: IconFont.VipIcon(),
                    leftText: Text("办理会员", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/vip');
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.BuyIcon(),
                    leftText: Text("会员状态", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/status');
                    },
                  ),
                  Container(
                    height: 10,
                    color: ColorConfig.LineColor,
                  ),

                  // CellWidget(
                  //   leftIcon: IconFont.VideoIcon(),
                  //   leftText: Text("成人视频", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                  //   onTap: () {
                  //     Modular.to.pushNamed('/video');
                  //   },
                  // ),
                  CellWidget(
                    leftIcon: IconFont.VideoIcon(),
                    leftText: Text("快手视频", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/video');
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.NewsIcon(),
                    leftText: Text("最新写真", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/recently');
                    },
                  ),
                  Container(
                    height: 10,
                    color: ColorConfig.LineColor,
                  ),

                  CellWidget(
                    leftIcon: IconFont.PictureIcon(),
                    leftText: Text("图片收藏", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/singlegraph');
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.ModelIcon(color: ColorConfig.TextColor,size: Screen.setFontSize(20)),
                    leftText: Text("写真收藏", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/like');
                    },
                  ),
                  Container(
                    height: 10,
                    color: ColorConfig.LineColor,
                  ),

                  CellWidget(
                    leftIcon: IconFont.AboutIcon(),
                    leftText: Text("关于我们", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/about');
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.XieYiIcon(),
                    leftText: Text("用户协议", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    onTap: () {
                      Modular.to.pushNamed('/agreement');
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.UpdateIcon(),
                    leftText: Text("检测升级", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    rightText: Text("v"+Config.packageInfo.version,style: TextStyle(fontSize: Screen.setFontSize(12),color: Color.fromARGB(87, 0, 0, 0))),
                    onTap: () async {
                      bool status = await CheckUpdate().check(context);
                      if (!status) {
                        Toast.show("已是最新版", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                      }
                    },
                  ),
                  CellWidget(
                    leftIcon: IconFont.ClearIcon(),
                    leftText: Text("清除缓存", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
                    rightText: Text(cacheSize,style: TextStyle(fontSize: Screen.setFontSize(12),color: Color.fromARGB(87, 0, 0, 0))),
                    onTap: () async {
                      await showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) => WillPopScope(
                            onWillPop: () async => false,
                            child: AlertDialog(
                              title: Text("清除缓存"),
                              content: Container(
                                height: 110,
                                child: Column(
                                  children: [
                                    Text("此操作将删除您的【分类菜单】【图片缓存】【收藏记录】等所有内容，请慎重考虑！！"),
                                    SizedBox(height: 10,),
                                    Text("清除成功后将强制关闭应用，请自行重启",style: TextStyle(color: ColorConfig.NoActiveColor,fontSize: 14))
                                  ],
                                ),
                              ),
                              actions: [
                                FlatButton(
                                    child: Text('取消', style: TextStyle(color: ColorConfig.ThemeColor),),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    }
                                ),
                                FlatButton(
                                    child: Text("清除", style: TextStyle(color: ColorConfig.NoActiveColor),),
                                    onPressed: () async {
                                      Navigator.of(context).pop();
                                      bool status = await ClearCache.clearCache();
                                      await ComputeCache();
                                      if (status) {
                                        exit(0);
                                      }
                                    }
                                ),
                              ],
                            ),
                          )
                      );
                    },
                  ),
                  SizedBox(height: 40,),
                  InkWell(
                    onTap: () async {
                      await SPreferences().setBool("isLogin", false);
                      Modular.to.pushReplacementNamed('/login');
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: Screen.setFontSize(15),right: Screen.setFontSize(15)),
                      height: Screen.setHeight(35),
                      alignment: Alignment.center,
                      width: Screen.width(context),
                      decoration: BoxDecoration(
                          color: ColorConfig.ThemeColor,
                          borderRadius: BorderRadius.circular(5)
                      ),
                      child: Text(
                        "退 出 登 录",
                        style: TextStyle(
                            color: ColorConfig.WhiteBackColor, fontSize: Screen.setFontSize(16)
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: Screen.setHeight(40),),
                ],
              ),
            )
          ),

          Tool.userInfo()['userType'] == "1" ? Positioned(
            right: 15,
            top: 10,
            child: InkWell(
              onTap: () {
                shareText();
              },
              child: Row(
                children: [
                  Text("分享赚钱", style: TextStyle(color: ColorConfig.WhiteBackColor,fontSize: 15)),
                ],
              ),
            ),
          ) : Container()
        ],
      ),
    );
  }
}
