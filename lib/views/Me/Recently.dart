import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';

class Recently extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Recently();
}

class _Recently extends State<Recently> {

  List<ItemList> result = [];
  int page = 1;

  @override
  void initState() {
    GetRecentlyData();
  }


  Future<AgentClassListData> GetRecentlyData() async {
    AgentClassListData res = await HomeService.GetRecently(params: { "page": page });
    print("========= ${res.total}");
    setState(() {
      result.addAll(res.list);
      page = int.parse(res.page);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("最近更新"),
        backgroundColor: ColorConfig.ThemeColor,
      ),
      body: EasyRefresh(
        header: BezierCircleHeader(backgroundColor: ColorConfig.ThemeColor),
        footer: BezierBounceFooter(backgroundColor: ColorConfig.ThemeColor),
        onRefresh: () async {
          print("刷新");
        },
        onLoad: () async {
          setState(() {
            page+=1;
            GetRecentlyData();
          });
        },
        child: ItemWidget(
            data: result,
            title: '',
            longPress: () {}
        ),
      ),
    );
  }
}