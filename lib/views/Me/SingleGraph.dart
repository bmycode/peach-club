import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';

class SingleGraph extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SingleGraph();
}

class _SingleGraph extends State<SingleGraph> {

  List<SingleGraphEntity> result = [];
  List<String> imgArr = [];

  @override
  void initState() {
    singleImg();
  }

  Future<List<Map<String, dynamic>>> singleImg () async {
    List<SingleGraphEntity> res = await LikeService.findAllImg();
    setState(() {
      result = res;
      res.forEach((v) {
        imgArr.add(v.src);
      });
    });
  }

  Future<void> deleteAlertDialog(String url) async {
    await Core.deleteDialog(context, () async {
      await LikeService.deleteImg(url);
      singleImg();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("单图收藏"),
        backgroundColor: ColorConfig.ThemeColor,
        actions: [
          Container(
            alignment:Alignment.center,
            padding: EdgeInsets.only(right: 15),
            child: Text("共 ${result.length ?? 0} 张"),
          )
        ],
      ),
      body: result.isNotEmpty ? ListView.builder(
        itemCount: result.length ?? 0,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onLongPress: () => deleteAlertDialog(result[index].src),
            onTap: () async {
              Navigator.of(context).push(new MaterialPageRoute(builder: (_) {
                return PhotoViewGalleryScreen(
                  images: imgArr,
                  index: index,
                  title: "",
                  time: "",
                  heroTag: index.toString(),
                );
              }));
            },
            child: CachedNetworkImg(result[index].src),
          );
        },
      ) : Container(
        height: Screen.setHeight(300),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.error_outline_outlined,size: 50,color: ColorConfig.ThemeColor,),
            SizedBox(height: 10,),
            Text("暂无收藏", style: TextStyle(color: ColorConfig.TextColor))
          ],
        ),
      ),
    );
  }
}