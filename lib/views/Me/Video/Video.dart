import 'package:flutter/material.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/views/Me/Video/player.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';

class Video extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Video();
}

// 短视频模块
class _Video extends State<Video> with AutomaticKeepAliveClientMixin<Video> {
  PageController pageController;

  String pcursor = "";

  @override
  void initState() {
    super.initState();
    pageController = PageController(
      initialPage: Config.currentIndex, //默认在第几个
      viewportFraction: 1, // 占屏幕多少，1为占满整个屏幕
      keepPage: true
    );

    GetAllVideoList();
  }

  /// 获取视频列表数据
  Future<VideoListEntity> GetAllVideoList({ String type = "swiper" }) async {
    print("GetAllVideoList");

    void getData () async {
      VideoListEntity result = await VideoService.GetVideoList(params: {
        "pcursor": pcursor
      });
      setState(() {
        Config.feeds.addAll(result.feeds);
        pcursor = result.pcursor;
      });

      print("feeds.length ${Config.feeds.length}");
    }

    if (type == "swiper") {
      print("swiper");
      if (Config.feeds.length == 0) {
        getData();
      }
    } else {
      print("more");
      getData();
    }

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.black,
      body: PageView(
        controller: pageController,
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        pageSnapping: true,
        onPageChanged: (int index) {
          setState(() {
            print("你切换了页面！ ============== ${index}");
            setState(() {
              Config.currentIndex = index;
              if (Config.currentIndex >= Config.feeds.length -1) {
                print("重新请企业数据.....");
                GetAllVideoList(type: "more");
              }
            });
          });
        },
        children: Config.feeds.map((e) {
          // print("e.photo.photoUrl ============ ${e.photo.photoUrl}");
          return Play(
            url: e.photo.photoUrl,
          );
        }).toList(),
      )
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    pageController.dispose();
  }
}
