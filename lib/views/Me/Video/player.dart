import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:video_player/video_player.dart';
import 'package:peach/config/_.dart';

class Play extends StatefulWidget {
  final String url;

  const Play({
     this.url
  });

  @override
  _Play createState() => _Play();
}

class _Play extends State<Play> {

  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    // print("widget.url：${widget.url}");
    _controller = VideoPlayerController.network(
      widget.url,
      videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
    );
    _controller.addListener(() {
      if(_controller.value.position == _controller.value.duration) {

      }
    });
    _controller.initialize().then((_) => setState(() {}));
    _controller.setLooping(true);
    _controller.play();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.black,
      alignment: Alignment.center,
      height: double.infinity,
      child: _controller.value.isInitialized
          ? GestureDetector(
        onTap: () {
          print(_controller.value.aspectRatio);
          if (_controller.value.isPlaying) {
            _controller.pause();
          } else {
            _controller.play();
          }
          setState(() {});
        },
        child: Stack(
          overflow: Overflow.visible,
          children: [
            AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  VideoPlayer( _controller),
                  VideoProgressIndicator(_controller, allowScrubbing: true, colors: VideoProgressColors(playedColor: ColorConfig.ThemeColor),),
                ],
              ),
            ),
            !_controller.value.isPlaying ? Positioned(
              top: 0,
              width: MediaQuery.of(context).size.width,
              child: InkWell(
                child: Center(
                  child: Icon(
                    Icons.play_arrow, size: 70, color: Colors.white,
                  ),
                ),
              ),
            ) : Text(""),
          ],
        ),
      )
          :  Center(
        child: SpinKitWave(color: ColorConfig.ThemeColor, itemCount: 3, size: 40),
      ),
    );
  }


}
