import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/R.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';
import 'package:toast/toast.dart';
import 'package:url_launcher/url_launcher.dart';

class BuyStatus extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BuyStatus();
}

class _BuyStatus extends State<BuyStatus> {

  List<Arr> orderList = [];

  @override
  void initState() {
    getOrderList();
  }

  Future<orderStatusEntity> getOrderList() async {
    orderStatusEntity res = await UserService.orderStatus(params: {
      "objectId": Tool.userInfo()['objectId']
    });
    print("============== ${res.code}");
    setState(() {
      orderList.addAll(res.arr);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("会员状态"),
        elevation: 0.0,
        backgroundColor: ColorConfig.ThemeColor,
      ),
      body: orderList.length == 0 ? Container(
        child: Text("没有记录"),
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 30),
      )
          : Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: Screen.setHeight(50)),
        child: Column(
          children: [
            orderList[0].activateStatus == "0" ? Icon(Icons.query_builder, size: 60,color: ColorConfig.ThemeColor,) : Icon(Icons.check, size: 60,color: ColorConfig.ThemeColor,),
            orderList[0].activateStatus == "0" ? SizedBox(height: 8,) : Container(),
            Text(orderList[0].activateStatus == "0" ? "等待您转账来激活账户" : "激活成功，可正常使用", style: TextStyle(fontSize: Screen.setFontSize(16)),),
            SizedBox(height: 5,),
            orderList[0].activateStatus == "0"
                ? Text("请加微信 ${orderList[0].superiorWeixin} 并转账，转账备注为订单号", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor),)
                : Text(""),
            SizedBox(height: 40,),

            CellWidget(
              leftIcon: IconFont.OrderIcon(size: 22),
              leftText: Text("订单号", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
              rightIcon: Text(""),
              rightText: Text("${orderList[0].orderNumber}（复制订单）",style: TextStyle(fontSize: Screen.setFontSize(13),color: Color.fromARGB(87, 0, 0, 0))),
              onTap: () {
                Core.copy(orderList[0].orderNumber);
                Toast.show("复制成功", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              },
            ),
            CellWidget(
              leftIcon: IconFont.WeiXinIcon(size: 22),
              leftText: Text("客服微信", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
              rightText: Text("${orderList[0].superiorWeixin}（跳转微信）",style: TextStyle(fontSize: Screen.setFontSize(13),color: Color.fromARGB(87, 0, 0, 0))),
              rightIcon: Text(""),
              onTap: () {
                Core.copy(orderList[0].superiorWeixin);
                Toast.show("复制成功", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                launch("weixin://");
              },
            ),

            Container(
              height: 10,
              color: ColorConfig.LineColor,
            ),

            CellWidget(
              leftIcon: IconFont.VipIcon(),
              rightIcon: Text(""),
              leftText: Text("VIP类型", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
              rightText: Text(Config.vipList[int.parse(orderList[0].vipLevel) - 1]['vipTitle'],style: TextStyle(fontSize: Screen.setFontSize(13),color: Color.fromARGB(87, 0, 0, 0))),
              onTap: () {},
            ),
            CellWidget(
              leftIcon: IconFont.BuyIcon(size: 22),
              rightIcon: Text(""),
              leftText: Text("VIP价格", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
              rightText: Text("￥${Config.vipList[int.parse(orderList[0].vipLevel) - 1]['vipMoney'].toString()}元",style: TextStyle(fontSize: Screen.setFontSize(13),color: ColorConfig.ThemeColor)),
              onTap: () {},
            ),
            CellWidget(
              leftIcon: IconFont.StatusIcon(),
              rightIcon: Text(""),
              leftText: Text("VIP状态", style: TextStyle(fontSize: Screen.setFontSize(14),color: ColorConfig.TextColor)),
              rightText: Container(
                padding: EdgeInsets.only(left: 6,right: 6,top: 3,bottom: 4),
                decoration: BoxDecoration(
                    color: ColorConfig.ThemeColor,
                    borderRadius: BorderRadius.circular((10.0))
                ),
                child: Text(orderList[0].activateStatus == "0" ? '未付款': '已付款',style: TextStyle(fontSize: Screen.setFontSize(12),color: ColorConfig.WhiteBackColor)),
              ),
              onTap: () {
              },
            ),
          ],
        ),
      ),
    );
  }
}