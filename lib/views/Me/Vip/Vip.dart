import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/R.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:toast/toast.dart';

class Vip extends StatefulWidget {

  @override
  State createState() => _Vip();
}

class _Vip extends State<Vip> {

  int currentVipType = 1;
  List<String> currentExplain;

  @override
  void initState() {
    setState(() {
      currentExplain = Config.vipList[currentVipType - 1]["vipExplain"];
    });
  }

  choiceVip(Map<String, dynamic> type) {
    print(type);
    setState(() {
      currentVipType = type["id"];
      currentExplain = type["vipExplain"];
    });
  }

  Future<void> placeOrder() async {
    RegisterEntity res = await UserService.placeOrder(params: {
      "objectId": Tool.userInfo()['objectId'],
      "vipLevel": Config.vipList[currentVipType - 1]['id'].toString()
    });
    Modular.to.pushNamed('/status');
    // if (res.code == 200) {
    //   Modular.to.pushNamed('/status');
    // } else {
    //   Toast.show(res.error, context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConfig.ThemeColor,
        title: Text("办理会员"),
        elevation: 0.0
      ),
      body: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: Screen.setHeight(70),
            padding: EdgeInsets.only(left: 15, right: 15),
            color: ColorConfig.ThemeColor,
            child: Row(
              children: [
                Image.asset(
                  R.libStaticImgViplogoPng,
                  width: Screen.setWidth(50),
                  height: Screen.setWidth(50),
                ),
                SizedBox(width: 10,),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(Tool.userInfo()['username'], style: TextStyle(color: ColorConfig.WhiteBackColor,fontSize: Screen.setFontSize(18)),),
                      Text("账户安全等级：安全",style: TextStyle(fontSize: Screen.setFontSize(11),color: ColorConfig.NoActiveColor),),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15, right: 15),
            width: Screen.width(context),
            height: Screen.setHeight(95),
            margin: EdgeInsets.only(top: 10),
            child: Column(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: Config.vipList.map((e) => InkWell(
                        onTap: () {
                          choiceVip(e);
                        },
                        child: Container(
                          width: currentVipType == e["id"] ? Screen.width(context) / 2: Screen.width(context) / 3.5,
                          margin: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: currentVipType == e["id"] ? ColorConfig.ThemeColor : ColorConfig.IconColor,
                                  width: currentVipType == e["id"] ? 1 : 0.5
                              ),
                              borderRadius: BorderRadius.circular((7.0))
                          ),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),topRight: Radius.circular(5.0)),
                                  color: ColorConfig.ThemeColor,
                                ),
                                alignment: Alignment.center,
                                padding: EdgeInsets.only(top: 5,bottom: 5),
                                child: Text(e['vipTitle'],style: TextStyle(fontSize: 14,color: ColorConfig.WhiteBackColor),),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("￥",style: TextStyle(fontSize: Screen.setFontSize(12),fontWeight: FontWeight.w700,color: ColorConfig.ThemeColor)),
                                    Text(e["vipMoney"].toString(),style: TextStyle(fontSize: Screen.setFontSize(24),fontWeight: FontWeight.w700,color: ColorConfig.ThemeColor)),
                                  ],
                                ),
                                height: Screen.setHeight(40),
                                alignment: Alignment.center,
                              ),
                              Container(
                                padding: EdgeInsets.only(bottom: 5),
                                child: Text("每天仅${e["vipDay"].toString()}元",style: TextStyle(fontSize: 10,color: ColorConfig.TextColor),),
                              )
                            ],
                          ),
                        ),
                      )).toList()
                  ),
                ),

              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("1：会员权益介绍",style: TextStyle(fontSize: Screen.setFontSize(18),fontWeight: FontWeight.w600),),
                SizedBox(height: 10,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: currentExplain != null ? currentExplain.map((e) =>
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Text(e,style: TextStyle(color: ColorConfig.TextColor),),
                      )
                  ).toList() : [],
                )
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(bottom: 50),
            padding: EdgeInsets.only(left: 15, right: 15,top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("2：购买提示",style: TextStyle(fontSize: Screen.setFontSize(18),fontWeight: FontWeight.w600),),
                SizedBox(height: 10,),
                Container(
                  margin: EdgeInsets.only(left: 15),
                  child: Column(
                    children: [
                      Text("1：当您点击办理按钮后，前往微信添加给出的客服微信号，然后向对方转账并填写转账备注为订单号！",style: TextStyle(color: ColorConfig.TextColor)),
                      SizedBox(height: 5,),
                      Text("2：转账成功后，请等待客服为您办理激活，一分钟左右即可办理成功。",style: TextStyle(color: ColorConfig.TextColor)),
                      SizedBox(height: 5,),
                    ],
                  ),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              placeOrder();
              // Modular.to.pushNamed("/status");
            },
            child: Container(
              margin: EdgeInsets.only(left: Screen.setFontSize(15),right: Screen.setFontSize(15)),
              height: Screen.setHeight(35),
              alignment: Alignment.center,
              width: Screen.width(context),
              decoration: BoxDecoration(
                  color: ColorConfig.ThemeColor,
                  borderRadius: BorderRadius.circular(20)
              ),
              child: Text(
                "￥${Config.vipList[currentVipType - 1]['vipMoney']}/${Config.vipList[currentVipType - 1]['vipTitle']}  点击办理",
                style: TextStyle(
                    color: ColorConfig.WhiteBackColor, fontSize: Screen.setFontSize(16)
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}