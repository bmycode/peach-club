import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';

class Mechanism extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Mechanism();
}

class _Mechanism extends State<Mechanism> with AutomaticKeepAliveClientMixin<Mechanism> {

  /// 获取机构列表的数据
  Future<List<MechanismResult>> GetMechanismData() async {
    return await MechanismService.GetMechanismData(params: {});
  }

  /// 组件
  Widget MechanismList(List<MechanismResult> res) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.all(Screen.setFontSize(15)),
        margin: EdgeInsets.only(top: 15),
        child: Wrap(
          runAlignment: WrapAlignment.spaceBetween,
          children: res.map((e) =>  Container(
            width: Screen.setWidth(86),
            margin: EdgeInsets.only(bottom: 20),
            child: InkWell(
              onTap: () {
                Modular.to.pushNamed("/mechanismList/${e.id}/${e.title}/mechanism");
              },
              child: Column(
                children: [
                  Text(e.title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ColorConfig.TitleColor,
                          fontSize: Screen.setFontSize(16))),
                  SizedBox(height: 10),
                  Text(
                    e.quantity,
                    style: TextStyle(
                        color: ColorConfig.ThemeColor,
                        fontSize: Screen.setFontSize(12)),
                  )
                ],
              ),
            ),
          )).toList()
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConfig.ThemeColor,
        automaticallyImplyLeading: false,
        title: Text("机构"),
      ),
      body: FutureBuilderWidget<List<MechanismResult>>(
        future: GetMechanismData,
        success: MechanismList,
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
