import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:toast/toast.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/widget/_.dart';

class MechanismList extends StatefulWidget {
  String id;
  String title = "";
  String type;

  int totalPage = 0;

  MechanismList({ this.id, this.title, this.type });

  @override
  State<StatefulWidget> createState() => _MechanismList();
}

class _MechanismList extends State<MechanismList> {
  int page = 1;
  List<ItemList> Result = [];

  @override
  void initState() {
    switch(widget.type) {
      case 'mechanism':
        GetmechanismList();
        break;
      case 'tags':
        getClassListData();
        break;
    }
  }

  /// 机构列表点击进入详情
  Future<void> GetmechanismList() async {
    AgentClassListData res = await MechanismService.GetmechanismList(params: { "id": widget.id, "page": page });
    setState(() {
      Result.addAll(res.list);
      widget.totalPage = int.parse(res.total);
      page = int.parse(res.page);
    });
  }

  Future<AgentClassListData> getClassListData() async {
    AgentClassListData res = await HomeService.GetClassListData(params: { 'id': widget.id, 'page': page });
    setState(() {
      Result.addAll(res.list);
      widget.totalPage = int.parse(res.total);
      page = int.parse(res.page);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorConfig.ThemeColor,
        title: Text(widget.title),
      ),
      body: EasyRefresh(
        header: BezierCircleHeader(backgroundColor: ColorConfig.ThemeColor),
        footer: BezierBounceFooter(backgroundColor: ColorConfig.ThemeColor),
        onRefresh: () async {
          print("刷新");
        },
        onLoad: () async {
          setState(() {
            page+=1;
            if (page > widget.totalPage) {
              Toast.show("没有数据", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            } else {
              GetmechanismList();
            }
          });
        },
        child: ItemWidget(
            data: Result,
            title: '',
            longPress: () {}
        ),
      ),
    );
  }
}