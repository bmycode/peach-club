import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';

class Model extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Model();

}

class _Model extends State<Model> with AutomaticKeepAliveClientMixin<Model>{

  Future<ModelResult> GetModelData() async {
    return await ModelService.GetModelData(params: {});
  }


  Widget ModelList (ModelResult res) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: Screen.setFontSize(15)),
            child: ModelWidget(data: res.hotModels, height: 25,),
          ),
          // Container(
          //   padding: EdgeInsets.only(top: Screen.setFontSize(15)),
          //   child: ModelWidget(title: "最新模特", data: res.newest, height: 25,),
          // )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorConfig.ThemeColor,
        automaticallyImplyLeading: false,
        title: Text("模特"),
      ),
      body: FutureBuilderWidget<ModelResult>(
        future: GetModelData,
        success: ModelList,
      )
    );
  }

  @override
  bool get wantKeepAlive => true;

}