import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:toast/toast.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/widget/_.dart';

class ModelInfo extends StatefulWidget {

  String title;
  String id;
  ModelInfo({ this.title, this.id });

  @override
  State<StatefulWidget> createState() => _ModelInfo();
}

class _ModelInfo extends State<ModelInfo> {


  ModelInfoResult Info = ModelInfoResult.fromJson({
    "current_page": 0, "total_page": 0, "modelInfo": {}, "data": [], "similar": []
  });

  @override
  void initState() {
    new Future.delayed(Duration(milliseconds: 500),() async {
      GetModelInfo();
    });
  }

  Future<ModelInfoResult> GetModelInfo() async {
    ModelInfoResult res = await ModelService.GetModelInfo(params: {
      "page": Info.currentPage,
      "id": widget.id
    });
    setState(() {
      Info.data.addAll(res.data);
      Info.similar = res.similar;
      Info.modelInfo = res.modelInfo;
      Info.totalPage = res.totalPage;
      Info.currentPage = res.currentPage;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: ColorConfig.ThemeColor,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        child: Info.data.isNotEmpty ? Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(15),
                child: Row(
                  children: [
                    ClipOval(
                      child: Image.network(
                        Info.modelInfo.portrait, width: Screen.setWidth(60),
                        height: Screen.setWidth(60), fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(Info.modelInfo.name, style: TextStyle(fontWeight: FontWeight.w500,fontSize: 16),),
                          SizedBox(height: 6,),
                          Text(Info.modelInfo.explain, style: TextStyle(fontSize: 12,color: ColorConfig.TextColor),)
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: OldItemWidget(title: "她的作品", data: Info.data,),
              ),
              Container(
                alignment: Alignment.center,
                child: IconButton(
                  onPressed: () async {
                    Info.currentPage += 1;
                    if (Info.currentPage > Info.totalPage || Info.totalPage == 99999) {
                      Toast.show("没有数据", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    } else {
                      GetModelInfo();
                    }
                  },
                  icon: Icon(Icons.keyboard_arrow_down,size: 40,color: ColorConfig.ThemeColor,),
                ),
              ),
              Container(
                child: ModelWidget(title: "类似模特", data: Info.similar, height: 25),
              ),
            ],
          ),
        ) : Container(
          height: Screen.setHeight(250),
          child: Center(
            child: SpinKitWave(color: ColorConfig.ThemeColor, itemCount: 3, size: 40),
          ),
        ),
      ),
    );
  }
}