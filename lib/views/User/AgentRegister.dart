import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/LoginEntity.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/UserService.dart';
import 'package:peach/utils/_.dart';
import 'package:toast/toast.dart';
import 'package:flutter/services.dart';

class AgentRegister extends StatefulWidget {

  @override
  State createState() => _AgentRegister();
}

class _AgentRegister extends State<AgentRegister> {

  // 手机号
  TextEditingController _phoneController = TextEditingController();
  // 密码
  TextEditingController _passwordController = TextEditingController();
  // 代理码
  TextEditingController _agentCodeController = TextEditingController();
  // 代理的微信
  TextEditingController _weiXinController = TextEditingController();

  bool isShowPassword = true;

  Future<void> agentUserRegisterAction() async {
    if (!Tool.isPhone(_phoneController.text)) {
      Toast.show("手机号不正确，请检查", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    if (_passwordController.text.length == 0) {
      Toast.show("密码不能为空", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    if (_agentCodeController.text.length == 0) {
      Toast.show("代理激活码不能为空", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    if (_weiXinController.text.length == 0) {
      Toast.show("代理微信号不能为空", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    RegisterEntity user = await UserService.agentUserRegister(params: {
      'userphone': _phoneController.text,
      'userpwd': _passwordController.text,
      'agentcode': _agentCodeController.text,
      'userweixin': _weiXinController.text
    });
    if (user.code == 200) {
      print("注册成功");
      Modular.to.pushReplacementNamed('/login');
    } else {
      print("注册失败");
      Toast.show(user.error, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: ColorConfig.ThemeColor,
        actions: [
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(right: 15),
            child: InkWell(
              onTap: () {
                Modular.to.pushNamed("/login");
              },
              child: Text("登录"),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          padding: EdgeInsets.only(
              top: Screen.setFontSize(30),
              left: Screen.setFontSize(30),
              right: Screen.setFontSize(30)),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "注册为代理",
                    style: TextStyle(fontSize: 30),
                  ),
                  SizedBox(height: 20,),
                  Text(
                    "1：请确保手机号的正确，否则功能无法正常使用",
                    style: TextStyle(fontSize: 15),
                  ),
                  InkWell(
                    onLongPress: () {
                      Clipboard.setData(ClipboardData(text: 'xy7145'));
                      Toast.show("微信号已复制，请使用微信添加好友", context, duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    },
                    child: Text(
                      "2：如果您想成为代理赚取更多的额外收益，那么请加总代理 微信：xy7145（长按复制） 进行申请即可。",
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  Text(
                    "3：【代理激活码】请找总代理获取并粘贴至下面输入框中！",
                    style: TextStyle(fontSize: 15),
                  ),
                  Text(
                    "4：【收款微信号】请务必填写您真实有效且经常在用的微信号",
                    style: TextStyle(fontSize: 15),
                  ),
                  SizedBox(
                    height: Screen.setHeight(20),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: Screen.setFontSize(80)),
                    child: Column(
                      children: [
                        TextField(
                          controller: _phoneController,
                          keyboardType: TextInputType.number,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide: BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入手机账户',
                          ),
                        ),
                        TextField(
                          controller: _passwordController,
                          keyboardType: TextInputType.visiblePassword,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          obscureText: isShowPassword,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              color: ColorConfig.TextColor,
                              onPressed: () {
                                setState(() => isShowPassword = !isShowPassword);
                              },
                              icon: isShowPassword
                                  ? Icon(Icons.remove_red_eye_rounded)
                                  : Icon(Icons.remove_red_eye_outlined),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide: BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入账户密码',
                          ),
                        ),
                        TextField(
                          controller: _agentCodeController,
                          keyboardType: TextInputType.number,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide: BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入代理激活码',
                          ),
                        ),
                        TextField(
                          controller: _weiXinController,
                          keyboardType: TextInputType.number,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide: BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入收款微信号',
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      await agentUserRegisterAction();
                    },
                    child: Container(
                        height: Screen.setHeight(35),
                        alignment: Alignment.center,
                        width: Screen.width(context),
                        decoration: BoxDecoration(
                            color: ColorConfig.ThemeColor,
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Text("注 册 为 代 理", style: TextStyle(color: ColorConfig.WhiteBackColor,fontSize: 18),)
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}