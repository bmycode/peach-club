import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/entity/_.dart';
import 'package:peach/service/_.dart';
import 'package:peach/utils/_.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Login extends StatefulWidget {
  @override
  State createState() => _Login();
}

class _Login extends State<Login> {
  // 手机号
  TextEditingController _phoneController = TextEditingController();

  // 密码
  TextEditingController _passwordController = TextEditingController();

  bool isShowPassword = true;

  Future<void> userLoginAction() async {
    if (!Tool.isPhone(_phoneController.text)) {
      Toast.show("手机号不正确，请检查", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    if (_passwordController.text.length == 0) {
      Toast.show("密码不能为空", context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      return;
    }

    LoginEntity user = await UserService.userLogin(params: {
      'username': _phoneController.text,
      'userpwd': _passwordController.text
    });
    if (user.code == 200) {
      print("账户密码正确");
      print("userType ===== ${user.userType}");
      print("userWeiXin ===== ${user.userWeiXin}");
      await SPreferences().setBool("isLogin", true);
      await SPreferences().setString("username", user.username);
      await SPreferences().setString("objectId", user.objectId);
      await SPreferences().setString("createdAt", user.createdAt);
      await SPreferences().setString("userType", user.userType);
      await SPreferences().setString("userWeiXin", user.userWeiXin);
      Modular.to.pushReplacementNamed('/init');
    } else {
      print("账户密码错误");
      Toast.show(user.error, context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: ColorConfig.ThemeColor,
        actions: [
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(right: 15),
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    Modular.to.pushNamed("/reg");
                  },
                  child: Text("用户注册"),
                ),
                SizedBox(width: 15,),
                InkWell(
                  onTap: () {
                    Modular.to.pushNamed("/agentReg");
                  },
                  child: Text("代理注册"),
                )
              ],
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          padding: EdgeInsets.only(
              top: Screen.setFontSize(30),
              left: Screen.setFontSize(30),
              right: Screen.setFontSize(30)),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "登录",
                    style: TextStyle(fontSize: 30),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "欢迎来到屁桃儿，请使用手机号登录",
                    style: TextStyle(fontSize: 15),
                  ),
                  Text(
                    "请确保手机号的正确性，否则部分功能无法正常使用",
                    style:
                        TextStyle(fontSize: 12, color: ColorConfig.TextColor),
                  ),
                  SizedBox(
                    height: Screen.setHeight(30),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: Screen.setFontSize(80)),
                    child: Column(
                      children: [
                        TextField(
                          controller: _phoneController,
                          keyboardType: TextInputType.number,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          decoration: InputDecoration(
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide:
                                  BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入手机账户',
                          ),
                        ),
                        TextField(
                          controller: _passwordController,
                          keyboardType: TextInputType.visiblePassword,
                          cursorRadius: Radius.circular(10),
                          cursorColor: ColorConfig.ThemeColor,
                          obscureText: isShowPassword,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              color: ColorConfig.TextColor,
                              onPressed: () {
                                setState(
                                    () => isShowPassword = !isShowPassword);
                              },
                              icon: isShowPassword
                                  ? Icon(Icons.remove_red_eye_rounded)
                                  : Icon(Icons.remove_red_eye_outlined),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              //选中时下边框颜色
                              borderSide:
                                  BorderSide(color: ColorConfig.TextColor),
                            ),
                            labelStyle: TextStyle(
                              color: ColorConfig.TextColor,
                            ),
                            labelText: '请输入账户密码',
                          ),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      userLoginAction();
                    },
                    child: Container(
                      height: Screen.setHeight(35),
                      alignment: Alignment.center,
                      width: Screen.width(context),
                      decoration: BoxDecoration(
                          color: ColorConfig.ThemeColor,
                          borderRadius: BorderRadius.circular(30)
                      ),
                      child: Text(
                        "登 录",
                        style: TextStyle(
                            color: ColorConfig.WhiteBackColor, fontSize: 18
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    alignment: Alignment.center,
                    child: Text("忘记密码"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
