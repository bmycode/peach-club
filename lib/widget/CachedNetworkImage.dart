import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';

/// 缓存网络图片
/// 第一次加载网络图，第二次从内存直接获取
/// TODO 可以缓存图片到硬盘
class CachedNetworkImg extends StatelessWidget {

  String url;

  CachedNetworkImg(this.url);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      placeholder: (context, url) => Container(
        width: 130,
        height: 80,
        child: Center(
          child: CircularProgressIndicator(
            backgroundColor: ColorConfig.ThemeColor,
            strokeWidth: 2,
          ),
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
      fit: BoxFit.cover,
      width: Screen.setWidth(375),
    );
  }

}