import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';

/// cell 组件
class CellWidget extends StatelessWidget {
  final Widget leftIcon;
  final Widget leftText;

  final Widget rightIcon;
  final Widget rightText;
  final Function onTap;
  final double padding;

  CellWidget({
    this.leftIcon,
    this.leftText,
    this.rightIcon = const Icon(Icons.arrow_right_rounded,color: ColorConfig.NoActiveColor,size: 30,),
    this.rightText = const Text(""),
    this.onTap,
    this.padding = 15
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          padding: EdgeInsets.only(left: Screen.setFontSize(padding),right: Screen.setFontSize(padding)),
          height: Screen.setHeight(40),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  child: Row(
                    children: [
                      leftIcon,
                      SizedBox(width: Screen.setWidth(10),),
                      leftText
                    ],
                  ),
                ),
              ),
              rightText,
              SizedBox(width: Screen.setWidth(10),),
              rightIcon
            ],
          ),
        ),
      ),
    );
  }
}