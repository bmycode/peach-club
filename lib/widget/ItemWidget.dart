import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:peach/entity/_.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';

class ItemWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ItemWidget();

   List<ItemList> data = []; // list 列表数据
   String title;  // 标题
   Function longPress; // 列表item被点击后的事件回调

   ItemWidget({
     this.data,
     // this.isShowTitle = true,
     this.title = "",
     this.longPress,
   });
}


class _ItemWidget extends State<ItemWidget> {


  Widget TipsItem({ String title}) {
    return title == 'null' ? Text('') : Container(
      padding: EdgeInsets.only(top: 2,bottom: 2,left: 5,right: 5),
      decoration: BoxDecoration(
        color: ColorConfig.ThemeColor,
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: Text(title,style: TextStyle(fontSize: Screen.setFontSize(10),color: ColorConfig.WhiteBackColor),),
    );
  }

  Widget itemTitle(String title) {
    return title.length == 0 ? Text('') : Container(
      margin: EdgeInsets.only(bottom: Screen.setFontSize(10)),
      child: Row(
        children: [
          Icon(Icons.ac_unit, color: ColorConfig.ThemeColor,size: Screen.setFontSize(16),),
          SizedBox(width: 5,),
          Text(title, style: TextStyle(fontSize: Screen.setFontSize(17),color: ColorConfig.TitleColor),)
        ],
      ),
    );
  }

  Widget ListItem({ ItemList data }) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      width: Screen.setWidth(172),
      child: Column(
        children: [
          InkWell(
            onLongPress: () => widget.longPress(data.id),
            onTap: () => Modular.to.pushNamed("/details/${data.id}"),
            child: Stack(
              alignment: Alignment.centerLeft,
              children: [
                Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadiusDirectional.circular(5)),
                  clipBehavior: Clip.antiAlias,
                  elevation: 0,
                  child: CachedNetworkImage(
                    imageUrl: data.imgSrc,
                    key: Key(data.id.toString()),
                    height: Screen.setHeight(248),
                    fit: BoxFit.cover,
                    errorWidget:(context, url, error) => new Icon(Icons.error),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  right: 10,
                  child: TipsItem(title: data.shuliang.toString()),
                )
              ],
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onLongPress: () => widget.longPress(data.id),
                  onTap: () => Modular.to.pushNamed("/details/${data.id}"),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(data.title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: ColorConfig.TitleColor,fontSize: Screen.setFontSize(15)),),
                      // Text(widget.data.user,style: TextStyle(color: ColorConfig.IconColor,fontSize: Screen.setFontSize(13))),
                      data.jigoulist.isEmpty ? Text('') : Column(
                        children: [
                          Text(data.jigoulist[0].name,style: TextStyle(color: ColorConfig.IconColor,fontSize: Screen.setFontSize(13))),
                          SizedBox(height: 4),
                        ],
                      ),
                    ],
                  ),
                ),
                data.biaoqianlist.isEmpty
                    ? Text("")
                    : Wrap(
                    spacing: 3,
                    runSpacing: 2,
                    children: data.biaoqianlist.map((v) => InkWell(
                      onTap: () {
                        Modular.to.pushNamed("/mechanismList/${v.id}/${v.name}/tags");
                      },
                      child: TipsItem(title: v.name),
                    )).toList()
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.data.isEmpty ? Container(
      height: MediaQuery.of(context).size.height / 2,
      child: Center(
        child: SpinKitWave(color: ColorConfig.ThemeColor, itemCount: 3, size: 40),
      ),
    )
    : ListView(
      shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      children: [
        GridView.builder(
            padding: EdgeInsets.only(left: 5,right: 5,top: 10),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 2 / 4.6,
            ),
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            itemCount: widget.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListItem(
                data: widget.data[index]
              );
            }
        )
      ],
    );
  }

}