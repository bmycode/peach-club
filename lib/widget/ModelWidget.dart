import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';
import 'package:peach/entity/_.dart';

//// 模特item组件封装
class ModelWidget extends StatelessWidget {

  final List<Res> data;
  final String title;
  final double height;

  ModelWidget({
    this.title,
    this.data,
    this.height: 0
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          // 如果标题没传，那么现实空，否则现实标题
          title == null
              ? Text('')
              : Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(bottom: Screen.setFontSize(20),top: Screen.setFontSize(13),left: Screen.setFontSize(15)),
            child: Text(
              title,
              style: TextStyle(
                  fontSize: Screen.setFontSize(17), color: ColorConfig.TitleColor,
                  fontWeight: FontWeight.w400
              ),
            ),
          ),
          Wrap(
            children: data.map((e) => Container(
              width: Screen.setWidth(75),
              margin: EdgeInsets.only(bottom: height),
              child: InkWell(
                onTap: () {
                  Modular.to.pushNamed('/modelInfo/${e.id}/${e.title}');
                },
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: e.preview,
                            width: Screen.setWidth(60),
                            height: Screen.setWidth(60),
                            fit: BoxFit.cover,
                            errorWidget:(context, url, error) => new Icon(Icons.error),
                          ),
                        ),
                        e.total.length == 0 ? Text("")
                            : Positioned(
                                bottom: 0,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: Screen.setFontSize(5),right: Screen.setFontSize(5),
                                      top: Screen.setFontSize(1),bottom: Screen.setFontSize(2)
                                  ),
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(0, 0, 0, 0.58),
                                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                                  ),
                                  child: Text(
                                    e.total,
                                    style: TextStyle(
                                        color: ColorConfig.WhiteBackColor,
                                        fontSize: Screen.setFontSize(11)
                                    ),
                                  ),
                                ),
                        )
                      ],
                    ),
                    SizedBox(height: Screen.setHeight(3),),
                    Text(
                      e.title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: ColorConfig.TextColor, fontSize: Screen.setFontSize(11)),
                    )
                  ],
                ),
              ),
            )).toList(),
          )
        ],
      ),
    );
  }

}

