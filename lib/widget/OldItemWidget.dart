import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:peach/entity/_.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';

class OldItemWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _ItemWidget();

  List<HomeEntityData> data;
  String title;
  IconData icon;
  Function longPress;

  OldItemWidget({
    this.data,
    this.title = "",
    this.icon,
    this.longPress,
  });
}


class _ItemWidget extends State<OldItemWidget> {


  Widget TipsItem({ String title}) {
    return title == 'null' ? Text('') : Container(
      padding: EdgeInsets.only(top: 2,bottom: 2,left: 5,right: 5),
      decoration: BoxDecoration(
        color: ColorConfig.ThemeColor,
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: Text(title,style: TextStyle(fontSize: Screen.setFontSize(10),color: ColorConfig.WhiteBackColor),),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Screen.setFontSize(15),right: Screen.setFontSize(15),bottom: Screen.setFontSize(15)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // 标题
          widget.title.length == 0 ? Text('') : Container(
            margin: EdgeInsets.only(bottom: Screen.setFontSize(10)),
            child: Row(
              children: [
                Icon(widget.icon, color: ColorConfig.ThemeColor,size: Screen.setFontSize(16),),
                SizedBox(width: 5,),
                Text(widget.title, style: TextStyle(fontSize: Screen.setFontSize(17),color: ColorConfig.TitleColor),)
              ],
            ),
          ),
          // 列表
          Wrap(
            alignment: WrapAlignment.spaceAround,
            children: widget.data.map((e) =>Container(
              width: Screen.setWidth(172),
              margin: EdgeInsets.only(bottom: 15),
              child: Column(
                children: [
                  InkWell(
                    onLongPress: () => widget.longPress(e.id),
                    onTap: () => Modular.to.pushNamed("/details/${e.id}"),
                    child: Stack(
                      alignment: Alignment.centerLeft,
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadiusDirectional.circular(5)),
                          clipBehavior: Clip.antiAlias,
                          elevation: 0,
                          child: CachedNetworkImage(
                            imageUrl: e.preview,
                            key: Key(e.id.toString()),
                            height: Screen.setHeight(248),
                            fit: BoxFit.cover,
                            errorWidget:(context, url, error) => new Icon(Icons.error),
                          ),
                        ),
                        Positioned(
                          bottom: 10,
                          right: 10,
                          child: TipsItem(title: e.total.toString()),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onLongPress: () => widget.longPress(e.id),
                          onTap: () => Modular.to.pushNamed("/details/${e.id}"),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(e.title,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(color: ColorConfig.TitleColor,fontSize: Screen.setFontSize(15)),),
                              Text(e.user,style: TextStyle(color: ColorConfig.IconColor,fontSize: Screen.setFontSize(13))),
                              e.source.length == 0 ? Text('') : Column(
                                children: [
                                  Text(e.source,style: TextStyle(color: ColorConfig.IconColor,fontSize: Screen.setFontSize(13))),
                                  SizedBox(height: 4),
                                ],
                              ),
                            ],
                          ),
                        ),
                        e.tags == null
                            ? Text("")
                            : Wrap(
                            spacing: 3,
                            runSpacing: 2,
                            children: e.tags.map((v) => InkWell(
                              onTap: () {
                                Modular.to.pushNamed("/mechanismList/${v.id}/${v.title}/tags");
                              },
                              child: TipsItem(title: v.title),
                            )).toList()
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )).toList(),
          )
        ],
      ),
    );
  }

}