import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:peach/config/_.dart';

class PhotoViewGalleryScreen extends StatefulWidget {
  List<String> images = [];
  int index = 0;
  String heroTag;
  String title;
  String time;
  PageController controller;

  PhotoViewGalleryScreen({
    Key key,
    @required this.images,
    this.index,
    this.title = "",
    this.time = "",
    this.controller,
    this.heroTag
  }) : super(key: key) {
    controller = PageController(initialPage: index);
  }

  @override
  _PhotoViewGalleryScreenState createState() => _PhotoViewGalleryScreenState();
}

class _PhotoViewGalleryScreenState extends State<PhotoViewGalleryScreen> {
  int currentIndex = 0;
  // String dropdownValue = '请选择';
  // List<String> settingList = ['请选择', '设置壁纸','设置锁屏','同时设置'];

  @override
  void initState() {
    super.initState();
    currentIndex = widget.index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            child: Container(
                color: Colors.black,
                child: PhotoViewGallery.builder(
                  scrollPhysics: const BouncingScrollPhysics(),
                  builder: (BuildContext context, int index) {
                    return PhotoViewGalleryPageOptions(
                      imageProvider: NetworkImage(widget.images[index]),
                      heroAttributes: widget.heroTag.isNotEmpty
                          ? PhotoViewHeroAttributes(tag: widget.heroTag)
                          : null,
                    );
                  },
                  itemCount: widget.images.length,
                  backgroundDecoration: null,
                  pageController: widget.controller,
                  enableRotation: true,
                  onPageChanged: (index) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                )),
          ),
          Positioned(
            //图片index显示
            top: MediaQuery.of(context).padding.top + 15,
            width: MediaQuery.of(context).size.width,
            child: Center(
              child: Text("${currentIndex + 1}P / ${widget.images.length}P",
                  style: TextStyle(color: Colors.white, fontSize: 16)),
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).padding.top + 15,
            width: MediaQuery.of(context).size.width,
            child: Center(
              child: Container(
                padding: EdgeInsets.only(left: 15,right: 5),
                child: Column(
                  children: [
                    Text(widget.title,style: TextStyle(color: Colors.white, fontSize: 14)),
                    SizedBox(height: 5,),
                    Text(widget.time,style: TextStyle(color: ColorConfig.TextColor, fontSize: 12))
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            //右上角关闭按钮
            right: 10,
            top: MediaQuery.of(context).padding.top,
            child: IconButton(
              icon: Icon(
                Icons.close,
                size: 30,
                color: Colors.white,
              ),
              onPressed: () {
                Modular.to.pop();
              },
            ),
          ),
        ],
      ),
    );
  }
}
