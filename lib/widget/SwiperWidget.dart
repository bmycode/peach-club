import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peach/config/_.dart';
import 'package:peach/utils/_.dart';

/// 轮播图组件的封装
class SwiperWidget extends StatelessWidget {
  List<Widget> child;
  double width;
  double height;
  bool autoplay;
  int autoplayDelay;

  SwiperWidget({
    @required this.child,
    this.width: double.infinity,
    this.height: 130,
    this.autoplay: true,
    this.autoplayDelay: 3000,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: height,
        width: width,
        child: Swiper(
          itemBuilder: (BuildContext context, int index) {
            //条目构建函数传入了index,根据index索引到特定图片
            return child[index];
          },
          itemCount: child.length != 0 ? child.length : 0,
          autoplay: autoplay,
          autoplayDelay: autoplayDelay,
          pagination: new SwiperPagination(
              builder: DotSwiperPaginationBuilder(
                size: Screen.setFontSize(7),
                activeSize: Screen.setFontSize(7),
                color: ColorConfig.TextColor,
                activeColor: ColorConfig.ThemeColor,
              )
          ),
        ),
      ),
    );
  }
}
