import 'dart:convert' as convert;

void main() async {

  var content = convert.utf8.encode("886");
  var digest = convert.base64Encode(content);
  print(digest);

  List<int> bytes = convert.base64Decode(digest);
  String result = convert.utf8.decode(bytes);

  print(result);

}